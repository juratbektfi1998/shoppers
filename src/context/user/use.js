import {useReducer, useCallback} from "react";
import get from "lodash.get";
import ApiUser from "services/controller/user";

export const actionTypes = {
    request: "user/request",
    requestSuccess: "user/requestSuccess",
    requestError: "user/requestError",
    reset: "user/reset",
    requestEditAddress: "user/requestEditAddress",
    requestEditAddressError: "user/requestEditAddressError",
    requestEditAddressSuccess: "user/requestEditAddressSuccess",
    changeStatus: "user/changeStatus",
    addToCard: "user/addToCard",
    deleteCard: "user/deleteCard",
    addToWishList: "user/addUserList",
    deleteWishList: "user/deleteWishList",
    getCardTotal: 'user/getCardTotal',
    getCardTotalError: 'user/getCardTotalError',
    getCardTotalSuccess: 'user/getCardTotalSuccess',
};

const initialState = {
    status: "initial",
    loading: null,
    success: {},
    error: {},
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.request:
            return {
                ...state,
                status: "loading",
            };
        case actionTypes.requestSuccess:
            return {
                ...state,
                success: action.data,
                status: "succes",
            };
        case actionTypes.requestError:
            return {
                ...state,
                error: action.data,
                status: "error",
            };
        case actionTypes.requestEditAddress:
            return {
                ...state,
                status: "edit_address_loading",
            };
        case actionTypes.requestEditAddressSuccess:
            const {newAddress} = action;
            return {
                ...state,
                success: {
                    ...state.success,
                    address: newAddress,
                },
                status: "edit_address_success",
            };
        case actionTypes.requestEditAddressError:
            return {
                ...state,
                status: "edit_address_error",
            };
        case actionTypes.changeStatus:
            return {
                ...state,
                status: action.status,
            };
        case actionTypes.addToWishList:
            let newWishlist = [...get(state, "success.wishlist", []), action.addedCardId];
            return {
                ...state,
                success: {
                    ...state.success,
                    wishlist: newWishlist,
                },
            };
        case actionTypes.deleteWishList:
            const {deletedWishId} = action;
            const finalResult = get(state, "success.wishlist", []).filter(
                (item) => item !== deletedWishId
            );
            return {
                ...state,
                success: {
                    ...state.success,
                    wishlist: finalResult,
                },
            };
        case actionTypes.addToCard:
            const {addedCard} = action;
            let newList = [...get(state, "success.cart"), addedCard];
            return {
                ...state,
                success: {
                    ...state.success,
                    cart: newList,
                },
            };
        case actionTypes.deleteCard:
            let updatedList =
                get(state, "success.cart", []).filter(
                    (item) => item["_id"] !== action.deletedId
                ) || [];

            return {
                ...state,
                success: {
                    ...state.success,
                    cart: updatedList,
                },
            };
        case actionTypes.getCardTotalSuccess:
            return {
                ...state,
                status: 'success',
                success: {
                    ...state.success,
                    cartTotal: action.cardTotal,
                }
            }
        case actionTypes.getCardTotal:
            return {
                ...state,
                error: action.data,
                status: 'loading',
            }
        case actionTypes.getCardTotalError:
            return {
                ...state,
                status: 'error'
            }
        case actionTypes.reset:
            return initialState;
        default:
            return state;
    }
};

const useUserContext = () => {
    const [user, userDispatch] = useReducer(userReducer, initialState);

    const getUserDetail = useCallback(async () => {
        userDispatch({type: actionTypes.request});
        const {data, success} = await ApiUser.getUserDetail();

        if (success)
            return userDispatch({
                type: actionTypes.requestSuccess,
                data: data.data,
            });

        userDispatch({type: actionTypes.requestError, payload: get(data, 'data')});
    }, []);

    const editUserAddress = async (body) => {
        userDispatch({type: actionTypes.requestEditAddress});
        const {data, success} = await ApiUser.editAddress(body);
        if (success) {
            userDispatch({
                type: actionTypes.requestEditAddressSuccess,
                newAddress: get(data, "data.address"),
            });
        } else {
            userDispatch({type: actionTypes.requestEditAddressError, data: ""});
        }

        setTimeout(() => {
            userDispatch({type: actionTypes.changeStatus, status: "success"});
        }, 100);
    };

    const addToCard = (card) =>
        userDispatch({type: actionTypes.addToCard, addedCard: card});

    const deleteCard = (cardId) =>
        userDispatch({type: actionTypes.deleteCard, deletedId: cardId});

    const addToWishList = (cardId) => {
        userDispatch({type: actionTypes.addToWishList, addedCardId: cardId})
    };

    const deleteWishItem = (wishId) => {
        userDispatch({type: actionTypes.deleteWishList, deletedWishId: wishId})
    };

    const getCardTotal = async () => {
        userDispatch({type: actionTypes.getCardTotal});
        const {data, success} = await ApiUser.getCardTotal();
        console.log(get(data, 'data'))
        if (success) {
            userDispatch({
                type: actionTypes.getCardTotalSuccess,
                cardTotal: get(data, "data"),
            });
        } else {
            userDispatch({type: actionTypes.getCardTotalError, data: ""});
        }

        setTimeout(() => {
            userDispatch({type: actionTypes.changeStatus, status: "success"});
        }, 100);
    }

    return [user, getUserDetail, editUserAddress, addToCard, deleteCard, addToWishList, deleteWishItem, userDispatch, getCardTotal];
};

export default useUserContext;
