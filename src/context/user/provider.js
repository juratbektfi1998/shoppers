import React, { useMemo } from "react";

import { UserContext } from "./context";
import useUserContext from "./use";
function UserProvider({ children }) {
  const [
    user,
    getUserDetail,
    editUserAddress,
    addToCard,
    deleteCard,
    addToWishList,
    deleteWishItem,
    userDispatch,
    getCardTotal,
  ] = useUserContext();

  const value = {
    state: {
      user,
    },
    actions: {
      getUserDetail,
      editUserAddress,
      addToCard,
      deleteCard,
      addToWishList,
      deleteWishItem,
      userDispatch,
      getCardTotal,
    },
  };

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
}

export { UserProvider };
