import React from "react";

import { LoginContext } from "./context";
import useLogin from "./use";
function LoginProvider({ children }) {
  const [loginData, login, dispatch] = useLogin();

  const value = {
    state: {
      loginData,
    },
    actions: {
      login,
      dispatch
    },
  };

  return (
    <LoginContext.Provider value={value}>{children}</LoginContext.Provider>
  );
}

export { LoginProvider };
