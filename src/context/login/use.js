import useRequest from "hooks/useRequest";
import { useReducer } from "react";
import ApiUser from "services/controller/user";
import { useHistory } from "react-router-dom";
import browserStorage from "services/browserStorage";
import get from 'lodash.get'

export const actionTypes = {
  request: "login/request",
  requestSuccess: "login/requestSuccess",
  requestError: "login/requestError",
  reset: "login/reset",
};

const initialState = {
  status: "initial",
  loading: null,
  success: {},
  error: {},
};

const loginReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.request:
      return {
        ...state,
        status: "loading",
      };
    case actionTypes.requestSuccess:
      return {
        ...state,
        success: action.data,
        status: "success",
      };
    case actionTypes.requestError:
      return {
        ...state,
        status: "error",
      };
    case actionTypes.reset:
      return initialState;
    default:
      return state;
  }
};

const useLogin = () => {
  const [loginData, loginDispatch] = useReducer(loginReducer, initialState);

  const login = async (values) => {
    loginDispatch({ type: actionTypes.request });

    const { data, success } = await ApiUser.login(values);

    if (success) {
      browserStorage.set("token", data["token"]);
      
      browserStorage.set("role", get(data, "data.role"));
      loginDispatch({ type: actionTypes.requestSuccess, data: data?.data });
    } else {
      loginDispatch({ type: actionTypes.requestError });
    }
  };

  return [loginData, login, loginDispatch];
};

export default useLogin;
