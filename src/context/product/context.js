import { createContext } from "react";

export const ProductContext = createContext({ state: {}, actions: {} });
