import React, { useEffect, useMemo, useCallback, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { ProductContext } from "./context";
import useRequest from "../../hooks/useRequest";
import ApiProduct from "../../services/controller/product";
import ApiCategory from "../../services/controller/category";

import notification from "components/notification";

import { requestActionTypes } from "hooks/useRequest";

function ProductProvider({ children }) {
  const [inputValue, setInputValue] = useState("");
  const [counter, setCounter] = useState(0);
  const [products, getProducts, productDispatch] = useRequest();
  const [searchedValuesState, searchValues, searchDispatch] = useRequest();
  const history = useHistory();

  const changeHandler = useCallback(
    (e) => {
      setInputValue(e.target.value);
    },
    [inputValue]
  );

  const config = useMemo(() => {
    if (products.status === "success") {
      return {
        isHave: products.success.length % 20 === 0,
        page: products.success.length / 20
      };
    } else {
      return {
        isHave: false,
        page: 0,
      };
    }
  }, [products]);

  // console.log

  const loadMore =useCallback(async () => {
      console.log(config);
    if (config.isHave) {
      await searchValues(ApiProduct.searchByName, {
        q: inputValue,
        var: config.page,
      });
    }
  },[inputValue, config])

  const search = useCallback(async () => {
    if (!inputValue) return;
    await getProducts(ApiProduct.searchByName, { q: inputValue, var: 0 });
    history.push("/productlist");
  }, [counter, inputValue]);

  const value = useMemo(() => {
    return {
      state: {
        products,
        inputValue,
        config
      },
      actions: {
        search,
        loadMore,
        changeHandler,
      },
    };
  }, [products, inputValue, config]);

  useEffect(() => {
    if (
      searchedValuesState.status === "success" &&
      products.status === "success"
    ) {
      productDispatch({
        type: requestActionTypes.success,
        data: [...products.success, ...searchedValuesState.success],
      });
    } else {
      notification
        .setType("error")
        .setMessage(searchedValuesState.error?.message)
        .alert();
    }
  }, [searchedValuesState]);

  return (
    <ProductContext.Provider value={value}>{children}</ProductContext.Provider>
  );
}

export default ProductProvider;
