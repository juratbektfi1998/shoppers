import React, { useEffect } from "react";
import Footer from "./components/Footer/Footer";
import TopNav from "./components/TopNav/TopNav";
import Routes from "./Routes";

import "./App.scss";

function App() {
  return (
    <div>
      <TopNav />
      <main className="main-box-app">
        <Routes />
      </main>
      <Footer />
    </div>
  );
}

export default App;
