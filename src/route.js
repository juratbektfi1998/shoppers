import React from "react";

import HomePage from "./pages/HomePage/HomePage";
import ProductDetail from "./pages/ProductDetail/ProductDetail";
import OtherPage from "./layouts/OtherPage/OtherPage";
import ProductList from "./pages/ProductList/ProductList";
import FaqPage from "./pages/FaqPage/FaqPage";
import CardPage from "./pages/CardPage/CardPage";
import WishListPage from "./pages/WishListPage/WishListPage";
import CheckOutPage from "./pages/CheckOutPage/CheckOutPage";
import UserAccount from "./pages/UserAccount/UserAccount";
import MyAccount from "./pages/MyAccount/MyAccount";
import Login from "./components/FormComponent/Login/Login";
import Register from "./components/FormComponent/SignUp/SignUp";
import FooterSectionOne from "components/FooterPage/FooterSectionOne";

const routes = [
  {
    component: HomePage,
    exact: true,
    path: "/",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: false,
  },
  {
    component: Login,
    exact: true,
    path: "/login",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: false,
  },
  {
    component: Register,
    exact: true,
    path: "/register",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: false,
  },
  {
    component: ProductList,
    exact: true,
    path: "/productlist/:categoryId?",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: true,
  },
  {
    component: ProductDetail,
    exact: true,
    path: "/productdetail/:categoryId/:id",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: false,
  },
  {
    component: OtherPage,
    exact: true,
    path: "/other",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: true,
  },

  {
    component: FaqPage,
    exact: true,
    path: "/faq",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: true,
  },
  {
    component: CardPage,
    exact: true,
    path: "/card",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: true,
  },
  {
    component: WishListPage,
    exact: true,
    path: "/wishList",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: true,
  },
  {
    component: CheckOutPage,
    exact: true,
    path: "/checkout",
    roles: ["CUSTOMER"],
    private: true,
  },
  {
    component: UserAccount,
    exact: true,
    path: "/useraccount",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: true,
  },
  {
    component: MyAccount,
    exact: true,
    path: "/myaccount",
    roles: ["CUSTOMER"],
    private: true,
  },
  {
    component: FooterSectionOne,
    exact: true,
    path: "/footerPage",
    roles: ["ADMIN", "SELLER", "CUSTOMER"],
    private: false,
  },
];

export default routes;
