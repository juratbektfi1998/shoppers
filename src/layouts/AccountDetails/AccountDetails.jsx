import { ErrorMessage, Field, Form, Formik } from "formik";
import { MDBBtn, MDBCol, MDBRow } from "mdbreact";
import React, { useMemo, useContext, useState } from "react";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { UserContext } from "context/user/context";
import "./AccountDetails.scss";
import { error } from "jquery";

const validationSchema = Yup.object().shape({
  firstName: Yup.string().required("Required!"),
  email: Yup.string().email("Email is not valid !").required("Required!"),
});

function AccountDetails(props) {
  const {
    state: { user },
  } = useContext(UserContext);
  
  const [validateError, setValidateError] = useState({});

  const defaultValues = useMemo(() => {
    const { success } = user;
    return {
      firstName: success.name,
      email: success.email,
      fileName: "",
    };
  }, [props.data]);

  const { register, handleSubmit, watch, errors } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: defaultValues,
  });

  const valueValidate = (values) => {
    const {firstName, newPassword, oldPassword} = values;
    const err = {};
    if(firstName.length < 3) {
      err.firstName = 'min Length 3'
    } else {
      err.firstName = null;
    }
    if(newPassword.length < 6) {
      err.newPassword = "min Password 6 characters"
    } else {
      err.newPassword = null;
    }
    if(oldPassword.length < 6) {
      err.oldPassword = "min Password 6 characters"
    } else {
      err.oldPassword = null;
    }
    if(newPassword !== oldPassword) {
      err.password = "check the code again"
    } else {
      err.password = null;
    }
    setValidateError({...err});
    
    return (err.firstName || err.newPassword || err.oldPassword) ? false : true;
  }

  const onSubmit = (values) => {
    const validate = valueValidate(values);
    if(validate) {
      console.log(validateError)
      const {firstName, email, fileName, oldPassword, newPassword} = values;
      const formData = new FormData()
      formData.set('name', firstName)
      formData.set('email', email)
      formData.set('oldPassword', oldPassword)
      formData.set('newPassword', newPassword)
      if (fileName.length > 0) {
        formData.append('profile_image', fileName[0])
      }
      props.editAccountDetails(formData)
    } else {
      // console.log(values);
    }
  };

  return (
    <div className="border rounded overflow-hidden px-md-5 py-md-4 p-3">
      <h5 className="font-weight-bold">My Info</h5>

      <form onSubmit={handleSubmit(onSubmit)} className="needs-validation error-messageffff">
        <div className="mb-3">
          {" "}
          <label htmlFor="firstName" className="grey-text">
            First name
          </label>
          <input
            name="firstName"
            type="text"
            id="firstName"
            className="form-control"
            placeholder="First name"
            required
            ref={register}
          />
          {validateError.firstName && <p>{validateError.firstName}</p>}
          <div className="valid-feedback">Looks good!</div>
          <p>{errors.firstName?.message}</p>
        </div>

        <div className="mb-3">
          {" "}
          <label htmlFor="email" className="grey-text">
            email
          </label>
          <input
            name="email"
            type="email"
            id="email"
            className="form-control"
            placeholder="email"
            required
            ref={register}
          />
          <div className="valid-feedback">Looks good!</div>
          <p>{errors.email?.message}</p>
        </div>

          <div className="mb-3">
            {" "}
            <label htmlFor="password" className="grey-text">
              Old Password
            </label>
            <input
              name="oldPassword"
              type="password"
              id="password"
              className="form-control"
              placeholder="old password"
              ref={register}
              required
            />
            {validateError.oldPassword && <p>{validateError.oldPassword}</p>}
            {validateError.password && <p>{validateError.password}</p>}
            <div className="valid-feedback">Looks good!</div>
            <p>{errors.oldPassword?.message}</p>
          </div>

        <div className="mb-3">
          {" "}
          <label htmlFor="email" className="grey-text">
            New Password
          </label>
          <input
            name="newPassword"
            type="password"
            className="form-control"
            placeholder="new password"
            ref={register}
            required

          />
          {validateError.newPassword && <p>{validateError.newPassword}</p>}
          {validateError.password && <p>{validateError.password}</p>}
          <div className="valid-feedback">Looks good!</div>
          <p>{errors.email?.message}</p>
        </div>
        <div className="mb-3">
          <label htmlFor="fileName">Enter file</label>
          <div class="custom-file">
            <input
              type="file"
              class="form-control custom-file-input"
              name="fileName"
              id="fileName"
              accept="image/*"
              ref={register}
              required
            />
            <label class="custom-file-label" for="customFile">
              Choose file
            </label>
          </div>

          <p>{errors.fileName?.message}</p>
        </div>
        <button type="submit" className="my-button-adwawdoi">
          {" "}
          submit{" "}
        </button>
      </form>

      <br />
    </div>
  );
}

export default AccountDetails;
