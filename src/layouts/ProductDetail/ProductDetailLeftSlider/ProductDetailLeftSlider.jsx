import React, {useState, useEffect} from "react";
import "./ProductDetailLeftSilder.scss";
import Slider from "react-slick";
import {BASE_URL} from "services/constants";

function ProductDetailLeftSlider({images}) {
    const [nav, setNav] = useState({nav1: null, nav2: null});
    let slider1;
    let slider2;

    useEffect(() => {
        const someFunc = () => {
            const myNav = {
                nav1: slider1,
                nav2: slider2,
            };
            setNav(myNav);
        };

        someFunc();
    }, []);
    return (
        <div className="product-detail-left-slider">
            {" "}
            <div className="my-product-sec-tree">
                <div className="my-product-list">
                    <div>
                        <Slider asNavFor={nav.nav2} ref={(slider) => (slider1 = slider)}>
                            {images && images.map((image, key) => (
                                <div className="my-product-img" key={key}>
                                    <div className="my-img-box">
                                        <img
                                            src={BASE_URL + image}
                                            alt="img1"
                                        />
                                    </div>
                                </div>
                            ))}
                        </Slider>
                        <Slider
                            asNavFor={nav.nav1}
                            ref={(slider) => (slider2 = slider)}
                            slidesToShow={5}
                            swipeToSlide={true}
                            focusOnSelect={true}
                        >
                            {images && images.map((image, key) => (
                                <div className="my-product-img" key={key}>
                                    <div className="my-img-box">
                                        <img
                                            src={BASE_URL + image}
                                            alt="img11"
                                        />
                                    </div>
                                </div>
                            ))}
                        </Slider>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ProductDetailLeftSlider;
