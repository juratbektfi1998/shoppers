import React from "react";
import "./ProductDetailBottomSilder.scss";
import Slider from "react-slick";
import ReactStars from "react-rating-stars-component";
import {Link} from "react-router-dom";
import {getLang} from "utils/getLang";
import {BASE_URL} from "services/constants";

function ProductDetailBottomSlider({related}) {
    let settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };
    const {status, success: {data}} = related;
    const lang = getLang();
    const ratingChanged = (newRating) => {
    };

    if (status === "error") return <div>error</div>;
    if (status === "loading") return <div>loading</div>;

    const getPrice = (price, discount) => {
        return (price - (discount / 100) * price)
    };

    return (
        <div className="product-detail-bottom-slider">
            <div className="my-product-sec-six">
                <div className="border-bottom-hr">
                    <div>Related products</div>
                    <div/>
                </div>
                <div className="my-slaydir-container">
                    <div>
                        <Slider {...settings}>
                            {data && data.map((product, key) => (
                                <Link key={key}
                                      to={`/productdetail/${product['category']['category']}/${product['_id']}`}
                                      className="my-product-img">
                                    <div className="my-img-box">
                                        <img
                                            src={BASE_URL + product['images'][0]}
                                            alt=""
                                        />
                                        <div className="my-rating">
                                            <div className="my-rating-1">
                                                <p>{product['category']['name'][`name_${lang}`]}</p>
                                                <h3>{product['title']}</h3>
                                                <div>
                                                    <ReactStars
                                                        value={product['rating'] || 0}
                                                        count={5}
                                                        onClick={ratingChanged}
                                                        size={24}
                                                        activeColor="#ffd700"
                                                    />
                                                </div>
                                            </div>
                                            <div className="my-rating-2">
                                                {product['discount'] ? (
                                                    <>
                                                        <p>
                                                            <strike>
                                                                {product['purchaseValute'].toLowerCase() === 'usd' ?
                                                                    ('$' + product.purchasePrice) : (product.purchasePrice + ' ' + product.purchaseValute)}
                                                            </strike>
                                                        </p>
                                                        <p>{product['purchaseValute'].toLowerCase() === 'usd' ?
                                                            ('$' + getPrice(product.purchasePrice, product['discount']['percent']))
                                                            : (getPrice(product.purchasePrice, product['discount']['percent']) + ' ' + product.purchaseValute)}
                                                        </p>
                                                    </>
                                                ) : (
                                                    <>
                                                        <p>
                                                        </p>
                                                        <p>{product['purchaseValute'].toLowerCase() === 'usd' ?
                                                            ('$' + product.purchasePrice) : (product.purchasePrice + ' ' + product.purchaseValute)}
                                                        </p>
                                                    </>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            ))}
                        </Slider>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ProductDetailBottomSlider;
