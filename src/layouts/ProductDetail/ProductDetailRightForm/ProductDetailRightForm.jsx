import React, { useState, useContext } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { UserContext } from "context/user/context";
import get from "lodash.get";
import notification from "components/notification";
import "./ProductDetailRightForm.scss";

import { ADMIN, CUSTOMER, SELLER } from "utils/roles";

function ProductDetailRightForm({ data, addToCard, addWishList }) {
  const { t } = useTranslation();
  const [count, setCount] = useState(1);
  const [color, setColor] = useState();
  const [size, setSize] = useState();
  const history = useHistory();
  const adminToken = localStorage.getItem("token");

  const {
    state: { user },
  } = useContext(UserContext);

  const productCount = (e) => {
    if (e === 0) {
      if (count === 0) return;
      setCount(count - 1);
    } else if (e === 1) {
      setCount(count + 1);
    }
  };

  const mySetColor = (col) => {
    setColor(col);
  };

  const AddToCard = () => {
    if (get(user, "success.role",'').toUpperCase() !== CUSTOMER && adminToken) {
      notification
        .setType("error")
        .setMessage("Admin va Seller Product qusholmaydi")
        .alert();
      return;
    }
    if (adminToken && count > 0) {
      if (size === 'Select the size' || color === 'Select the color' || !size || !color){
        notification.setMessage("Color and Size is required").setType("error").alert();
      } else {
        addToCard(data["_id"], count, color, size);
      }
    } else {
      history.push("/login");
    }
  };

  const AddToWishList = () => {
    if (get(user, "success.role",'').toUpperCase() !== CUSTOMER && adminToken) {
      notification
          .setType("error")
          .setMessage("Admin va Seller Istaklar ro`yxatiga qo`sha olmaydi")
          .alert();
      return;
    }
    if (adminToken) {
      addWishList(data["_id"]);
    } else {
      history.push("/login");
    }
  };

  return (
    <div className="product-detail-right-form">
      <div className="my-product-sec-five">
        <div className="my-box-content">
          <p>
            {t(`ProductDetailRightForm.Titul`)}:{" "}
            <span>
              {data.purchaseValute.toLowerCase() === "usd"
                ? "$" + data.purchasePrice
                : data.purchasePrice + " " + data.purchaseValute}
            </span>
          </p>

          <p>
            {t(`ProductDetailRightForm.Titul2`)}:{" "}
            <span>{data["soldBy"]["name"]}</span>
          </p>
          <p>
            {t(`ProductDetailRightForm.Titul3`)}: <span>{data["sold"]}</span>
          </p>
          <p>
            {t(`ProductDetailRightForm.Titul4`)}:{" "}
            <span>{data["quantity"]}</span>
          </p>
          <p className="qwerdsa-dawnjk">
            Size:
            <select class="mynyselect-afr" onChange={(e)=>setSize(e.target.value)}>
             <option>Select the size</option>
              {get(data, 'sizes', []).map((size, index)=>(
                <option key={index} value={size}>{size}</option>
              ))}
            </select>
          </p>
          <p>{t(`ProductDetailRightForm.Titul5`)}:</p>
          <div className="product-colors qwerdsa-dawnjk">
            <select class="mynyselect-afr" onChange={(e)=>setColor(e.target.value)}>
              <option>Select the color</option>
              {get(data, 'colors', []).map((color, index)=>(
                <option key={index} value={color}>{color}</option>
              ))}
            </select>
          </div>
          <p>{t(`ProductDetailRightForm.Titul6`)}:</p>
          <div className="my-product-count">
            <div onClick={() => productCount(0)} className={color}>
              <i className="fa fa-minus" />
            </div>
            <div>{count}</div>
            <div onClick={() => productCount(1)} className={color}>
              <i className="fa fa-plus" />
            </div>
          </div>
          <div onClick={AddToCard} className="add-to-carddawpdamap">
            ADD TO CARD
          </div>
          <div onClick={AddToWishList} className="add-to-carddawpdamapwishlist">
            ADD TO WISHLIST
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductDetailRightForm;
