import React from "react";
import "./ProductDetailMidInfo.scss";
import {Tab, Tabs, TabList, TabPanel} from "react-tabs";
import ChatPage from '../../../components/Chats';
import {getLang} from "utils/getLang";

function ProductDetailMidInfo({data, GetComments, comments, CreateComment, ReplyComment}) {
    const lang = getLang();

    return (
        <div className="product-detail-mid-info">
            <div className="my-product-sec-four">
                <div className="content-top">
                    <h2>
                        {data.title}
                    </h2>
                    <hr/>
                    <h3>Quick description</h3>
                    <p>
                        {data['description'][`description_${lang}`]}
                    </p>
                </div>
                <div className="content-bottom">
                    <Tabs>
                        <TabList>
                            <Tab>Technical informations</Tab>
                            <Tab>Reviews</Tab>
                        </TabList>

                        <TabPanel>
                            <div className="my-table">
                                <table>
                                    {data && data.options.map((option, key) => (
                                        <tr key={key}>
                                            <th>{option['key']}</th>
                                            <td>{option['value']}</td>
                                        </tr>
                                    ))}
                                </table>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <ChatPage comments={comments} GetComments={GetComments} CreateComment={CreateComment} ReplyComment={ReplyComment}/>
                        </TabPanel>

                    </Tabs>
                </div>
            </div>
        </div>
    );
}

export default ProductDetailMidInfo;
