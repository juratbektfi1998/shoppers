import React from "react";
import { get } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import "./CardPageRight.scss";

function CardPageRight({ cards, course }) {
  const history = useHistory();
  const { t } = useTranslation();
  const {
    status,
    success: { data },
  } = cards;

  console.log(cards, "test");

  const getTotal = () => {
    let uz = Array.isArray(data)?data
      .filter((item) => item.valute === "uzs")
      .reduce((total, current) => total + current.qty * current.price, 0):0;
    let en =Array.isArray(data)? data
      .filter((item) => item.valute === "usd")
      .reduce((total, current) => total + current.qty * current.price, 0):0;

    return {
      uz,
      en,
    };
  };

  if (status === "error" || course.status === 'error') return <p>error</p>;
  if (status === "loading" || course.status === 'loading') return <p>loading</p>;

  const redirect = () => history.push("checkout");
  return (
    <div className="cart-page-two mt-5">
      <div className="cart-page-two-container">
        <h2>{t(`CardPageRight.Titul`)}</h2>
        <div className="my-grid my-p-bottom">
          <div>{t(`CardPageRight.Titul1`)}</div>
          <div className="x-text-m">
            {getTotal().en * get(course, 'success.data.valute.usd') + getTotal().uz} uzs
          </div>
        </div>

        <div className="ckeckout-now" onClick={redirect}>
          {t(`CardPageRight.Titul8`)}
        </div>
      </div>
    </div>
  );
}

export default CardPageRight;
