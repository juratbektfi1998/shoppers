import React, { useState } from "react";
import "./CardPageLeft.scss";
import { BASE_URL } from "services/constants";

function CardPageLeft(props) {
  const [productCount, setProductCount] = useState(false);
  const {
    editCard,
    deleteCard,
    cards: {
      status,
      success: { data },
    },
    edited,
  } = props;

  function handleSumChange(value) {
    props.onChangeSum(value);
  }

  const handleClick = (id, item) => {
    if (item === 1) {
      data.forEach((item) => {
        if (item["_id"] === id) {
          editCard(id, "+");
        }
      });
      setProductCount(!productCount);
      return;
    }
    if (item === 0) {
      data.forEach((item) => {
        if (item["_id"] === id && item.qty !== 0) {
          editCard(id, "-");
        }
      });
      setProductCount(!productCount);
      return;
    }

    return;
  };

  const DeleteCard = (id) => {
    deleteCard(id);
  };

  if (status === "error") return <p>error</p>;
  if (status === "loading") return <p>loading</p>;

  return (
    <div className="cart-page-one mt-5">
      <link
        rel="stylesheet"
        href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
        crossorigin="anonymous"
      />
      <div className="my-cart-container">
        <div className="product-menu">
          <div>PRODUCT</div>
          <div>PRICE</div>
          <div>QUANTITY</div>
          <div>TOTAL</div>
        </div>
        <div>
          {data &&
            data.map((item, key) => {
              return (
                <div key={key} className="product-one-list">
                  <div>
                    <div>
                      <img src={BASE_URL + item["image"]} alt="wfwefcwef" />
                    </div>
                    <h4>{item["title"]}</h4>
                    <p>
                      <p>
                        {item["valute"].toLowerCase() === "usd"
                          ? "$" + item["price"]
                          : item["price"] + 'so`m'}
                      </p>
                      <div className="a-none">
                        <div className="my-count-product">
                          <div
                            onClick={() => {
                              handleClick(item["_id"], 0);
                            }}
                          >
                            <i className="fa fa-minus" />
                          </div>
                          <div>{item["qty"]}</div>
                          <div
                            onClick={() => {
                              handleClick(item["_id"], 1);
                            }}
                          >
                            <i className="fa fa-plus" />
                          </div>
                        </div>
                      </div>
                    </p>
                    <div className="b-none">
                      <div className="my-count-product">
                        <div
                          onClick={() => {
                            handleClick(item["_id"], 0);
                          }}
                        >
                          <i className="fa fa-minus" />
                        </div>
                        <div>{item["qty"]}</div>
                        <div
                          className={
                            edited.status === "loading" ? "hide" : "view"
                          }
                          onClick={() => {
                            handleClick(item["_id"], 1);
                          }}
                        >
                          <i class="fa fa-plus" />
                        </div>
                      </div>
                    </div>
                    <div className="ma-txt">
                      <p>{ item['valute'] === 'usd'&& '$'}{item["qty"] * item["price"]} { item['valute'] === 'uzs' && 'so`m'  }  </p>
                      <div className="product-delete a-none">
                        <i class="fal fa-trash-alt" />
                      </div>
                    </div>
                    <div
                      onClick={() => DeleteCard(item["_id"])}
                      className="product-delete b-none"
                    >
                      <i class="fal fa-trash-alt" />
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
}

export default CardPageLeft;
