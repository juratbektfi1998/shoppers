import React, { memo } from "react";
import { MDBListGroup, MDBListGroupItem, MDBContainer } from "mdbreact";
import "./MessageList.scss";
import get from 'lodash.get';
import moment from "moment";

function MessageList(props) {
  const { messageList, downloadFile } = props;

  console.log(messageList);

  return (
    <MDBContainer>
      <MDBListGroup style={{ width: "100%" }}>
        {messageList.map((messageItem) => (
          <MDBListGroupItem hover href="#">
            <div className="d-flex w-100 justify-content-between">
              {/*<h5 className="mb-1">List group item heading</h5>*/}
              <small className="text-muted">{moment(messageItem.createdAt).format("DD.MM.YYYY hh:mm")}</small>
            </div>
            <p className="mb-1">{messageItem.message}</p>
            {/*<small className="text-muted">Donec id elit non mi porta.</small>*/}
            <div className="my-delete-btn">
              {get(messageItem, 'image') && (
                <button className="btn btn-success btn-sm" onClick={()=>downloadFile(get(messageItem, 'image'))}>DOWNLOAD FILE</button>
              )}
              <button
                type="button"
                class="btn btn-danger btn-sm"
                onClick={() => props.delete(messageItem._id)}
              >
                DELETE
              </button>
            </div>
          </MDBListGroupItem>
        ))}
      </MDBListGroup>
    </MDBContainer>
  );
}

export default memo(MessageList);
