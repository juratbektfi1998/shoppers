import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useMemo, useContext } from "react";
import get from "lodash.get";
import useRequest from "hooks/useRequest";
import "./MyAddress.scss";

import * as Yup from "yup";
import { MDBCol, MDBRow, MDBBtn } from "mdbreact";
import Axios from "axios";
import { UserContext } from "context/user/context";
const validationSchema = Yup.object().shape({
  city: Yup.string().required("Required!"),
  address: Yup.string().required("Required!"),
  country: Yup.string().required("Required!"),
  postalCode: Yup.string().required("Required!"),
  phonenumber: Yup.string().required("Required!"),
  // s_City: Yup.string().required("Required!"),
  // s_AddressOne: Yup.string().required("Required!"),
  // s_Country: Yup.string().required("Required!"),
  // s_ZipCode: Yup.string().required("Required!"),
});

function MyAddress(props) {
  

  const {
    state: { user },
    actions: { editUserAddress },
  } = useContext(UserContext);

  console.log(user);

  const initialValues = useMemo(() => {
    const address = get(user, "success.address");
    return {
      city: address?.city || "",
      address: address?.address || "",
      country: address?.country || "",
      postalCode: address?.postalCode || "",
      phonenumber: address?.phonenumber || "",
    };
  }, [user]);

  
  

  return (
    <div className="my-address-box border rounded overflow-hidden px-md-5 py-md-4 p-3">
      <h5 className="font-weight-bold">My Info</h5>

      <Formik
        validationSchema={validationSchema}
        initialValues={initialValues}
        onSubmit={(values, actions) => {
          editUserAddress(values);
        }}
      >
        {({ errors, touched, values }) => (
          <Form>
            <MDBRow>
              <MDBCol md="6" sm="12" className="mt-3">
                
                <div className="mt-3">
                  <label htmlFor="city">
                    <small>
                      <b>City</b>
                    </small>
                  </label>
                  <Field
                    className="form-control rounded-pill"
                    type="text"
                    id="city"
                    name="city"
                    placeholder="Tyumen"
                  />
                  <small style={{ color: "red" }}>
                    <ErrorMessage name="city">
                      {(msg) => <div>{msg}</div>}
                    </ErrorMessage>
                  </small>
                </div>
                <div className="mt-3">
                  <label htmlFor="address">
                    <small>
                      <b>Address</b>
                    </small>
                  </label>
                  <Field
                    className="form-control rounded-pill"
                    type="text"
                    id="address"
                    name="address"
                    placeholder="Tyumen"
                  />
                  <small style={{ color: "red" }}>
                    <ErrorMessage name="address">
                      {(msg) => <div>{msg}</div>}
                    </ErrorMessage>
                  </small>
                </div>
                <div className="mt-3">
                  <label htmlFor="country">
                    <small>
                      <b>Country</b>
                    </small>
                  </label>
                  <Field
                    className="form-control rounded-pill"
                    type="text"
                    id="country"
                    name="country"
                    placeholder="Tyumen"
                  />
                  <small style={{ color: "red" }}>
                    <ErrorMessage name="country">
                      {(msg) => <div>{msg}</div>}
                    </ErrorMessage>
                  </small>
                </div>

                <MDBRow>
                  <MDBCol md="5" sm="12">
                    <div className="mt-3">
                      <label htmlFor="postalCode">
                        <small>
                          <b> ZIP Code </b>
                        </small>
                      </label>
                      <Field
                        className="form-control rounded-pill"
                        type="text"
                        id="postalCode"
                        name="postalCode"
                        placeholder="00055"
                      />
                      <small style={{ color: "red" }}>
                        <ErrorMessage name="postalCode">
                          {(msg) => <div>{msg}</div>}
                        </ErrorMessage>
                      </small>
                    </div>
                  </MDBCol>
                  <MDBCol md="7" sm="12">
                    <div className="mt-3">
                      <label htmlFor="phonenumber">
                        <small>
                          <b> Phone number </b>
                        </small>
                      </label>
                      <Field
                        className="form-control rounded-pill"
                        type="text"
                        id="phonenumber"
                        name="phonenumber"
                        placeholder="+998 99 999 99 99"
                      />
                      <small style={{ color: "red" }}>
                        <ErrorMessage name="phonenumber">
                          {(msg) => <div>{msg}</div>}
                        </ErrorMessage>
                      </small>
                    </div>
                  </MDBCol>
                </MDBRow>

                <div className="mt-3">
                  <MDBBtn color="light" className="rounded-pill mr-2 py-2" rounded>
                    Close
                  </MDBBtn>
                  <MDBBtn
                    color="warning"
                    className="rounded-pill py-2"
                    rounded
                    type="submit"
                  >
                    Save
                  </MDBBtn>
                </div>
              </MDBCol>

              {/* <MDBCol md="6" sm="12" className="mt-3">
                <h6 className="text-muted text-uppercase">SHIPPING ADRESS</h6>
                <hr />
                <div className="mt-3">
                  <label htmlFor="s_City">
                    <small>
                      <b>City</b>
                    </small>
                  </label>
                  <Field
                    className="form-control rounded-pill"
                    type="text"
                    id="s_City"
                    name="s_City"
                    placeholder="Tyumen"
                  />
                  <small style={{ color: "red" }}>
                    <ErrorMessage name="s_City">
                      {(msg) => <div>{msg}</div>}
                    </ErrorMessage>
                  </small>
                </div>
                <div className="mt-3">
                  <label htmlFor="s_AddressOne">
                    <small>
                      <b>Address 1</b>
                    </small>
                  </label>
                  <Field
                    className="form-control rounded-pill"
                    type="text"
                    id="s_AddressOne"
                    name="s_AddressOne"
                    placeholder="Tyumen"
                  />
                  <small style={{ color: "red" }}>
                    <ErrorMessage name="s_AddressOne">
                      {(msg) => <div>{msg}</div>}
                    </ErrorMessage>
                  </small>
                </div>
                <div className="mt-3">
                  <label htmlFor="s_AddressTwo">
                    <small>
                      <b>Address 2</b>
                    </small>
                  </label>
                  <Field
                    className="form-control rounded-pill"
                    type="text"
                    id="s_AddressTwo"
                    name="s_AddressTwo"
                    placeholder="Tyumen"
                  />
                  <small style={{ color: "red" }}>
                    <ErrorMessage name="s_AddressTwo">
                      {(msg) => <div>{msg}</div>}
                    </ErrorMessage>
                  </small>
                </div>
                <div className="mt-3">
                  <label htmlFor="s_Country">
                    <small>
                      <b>Country</b>
                    </small>
                  </label>
                  <Field
                    className="form-control rounded-pill"
                    type="text"
                    id="s_Country"
                    name="s_Country"
                    placeholder="Tyumen"
                  />
                  <small style={{ color: "red" }}>
                    <ErrorMessage name="s_Country">
                      {(msg) => <div>{msg}</div>}
                    </ErrorMessage>
                  </small>
                </div>

                <MDBRow>
                  <MDBCol md="5" sm="12">
                    <div className="mt-3">
                      <label htmlFor="s_ZipCode">
                        <small>
                          <b> ZIP Code </b>
                        </small>
                      </label>
                      <Field
                        className="form-control rounded-pill"
                        type="text"
                        id="s_ZipCode"
                        name="s_ZipCode"
                        placeholder="00055"
                      />
                      <small style={{ color: "red" }}>
                        <ErrorMessage name="s_ZipCode">
                          {(msg) => <div>{msg}</div>}
                        </ErrorMessage>
                      </small>
                    </div>
                  </MDBCol>
                  <MDBCol md="7" sm="12">
                    <div className="mt-3">
                      <label htmlFor="s_Phone">
                        <small>
                          <b> Phone number </b>
                        </small>
                      </label>
                      <Field
                        className="form-control rounded-pill"
                        type="text"
                        id="s_Phone"
                        name="s_Phone"
                        placeholder="+998 99 999 99 99"
                      />
                      <small style={{ color: "red" }}>
                        <ErrorMessage name="s_Phone">
                          {(msg) => <div>{msg}</div>}
                        </ErrorMessage>
                      </small>
                    </div>
                  </MDBCol>
                </MDBRow>

                <div className="mt-3 d-flex justify-content-end">
                  <MDBBtn color="primary" className="rounded-pill" rounded>
                    Edit
                  </MDBBtn>
                </div>
              </MDBCol> */}
            </MDBRow>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default MyAddress;
