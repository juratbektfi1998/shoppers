import React, { useEffect, useState, useContext } from "react";
import get from "lodash.get";
import { MDBAnimation, MDBContainer } from "mdbreact";
import { Link, useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { BASE_URL } from "services/constants";
import "./ProductTab.scss";

import { ProductContext } from "context/product/context";

function ProductTab({ products }) {
  const [t] = useTranslation();
  const [data, setData] = useState([]);
  const { status, success } = products;
  const { categoryId } = useParams();

  const {
    actions: { loadMore },
    state: { config },
  } = useContext(ProductContext);

  useEffect(() => {
    if (status === "success") {
      setData(success);
    } else {
      setData([]);
    }
  }, [products]);

  console.log(products);
  if (status === "error") return <div>error</div>;
  if (status === "loading") return <div>loading</div>;

  //   debugger;
  return (
    <div className="my-3">
      <MDBAnimation type="fadeInUp">
        <MDBContainer>
          <div className="product-tab">
            <div className="productslist">
              <div className="content">
                <div className="prodlist-tabs">
                  <div className="prodlist">
                    {data && data.length > 0 ? (
                      data.map((product, key) => (
                        <Link
                          to={`/productdetail/${get(
                            product,
                            "category.category"
                          )}/${product["_id"]}`}
                          key={key}
                        >
                          <div className={`box box${key + 1}`}>
                            <div className="border" />
                            <div
                              className="container"
                              style={{
                                height: "200px",
                                backgroundImage: `url('${BASE_URL}${product["images"][0]}')`,
                              }}
                            >
                              <h5>
                                {product["purchasePrice"] +
                                  " " +
                                  product["purchaseValute"]}
                              </h5>
                              <p>{product["title"]}</p>
                            </div>
                          </div>
                        </Link>
                      ))
                    ) : (
                      <h3>No products</h3>
                    )}
                  </div>
                </div>
              </div>
            </div>

            {config?.isHave ? (
              <div className="load-more my-3">
                <button className="load-more" onClick={loadMore}>
                  {t(`ProductTab.Titul`)}
                </button>
              </div>
            ) : (
              ""
            )}
          </div>
        </MDBContainer>
      </MDBAnimation>
    </div>
  );
}

export default ProductTab;
