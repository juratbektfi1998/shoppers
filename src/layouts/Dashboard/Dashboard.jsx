import React, { useState, useEffect } from "react";
import get from "lodash.get";
import "./Dashboard.scss";
import Slider from "react-slick";
import { BASE_URL } from "services/constants";
import { getLang } from "utils/getLang";

function Dashboard({ orders, statistics, ...props }) {
  const [defBef, setDefBef] = useState(2);
  const lang = getLang();

  function hundleClick(item) {
    setDefBef(item);
  }

  useEffect(() => {
    const {status, success} = orders;
    if (status === 'success') {
      switch (get(success, 'status')){
        case 'waiting':
          setDefBef(4)
          return
        case 'confirm':
          setDefBef(3)
          return
        case 'in progress':
          setDefBef(2)
          return
        case 'received':
          setDefBef(1)
          return;
      }
    }
  }, [orders.status])

  const settings = {
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 1000,
    autoplaySpeed: 3000,
    cssEase: "linear",
  };

  if (orders.status === "error") return <h1>Error</h1>;
  if (orders.status === "loading") return <h1>Loading...</h1>;

  return (
    <div>
      <div className="my-dashboard-page-one">
        <div className="my-dashboard-page-one-container">
          <h3 className="top-txt">Quick informations</h3>

          <div className="dashboard-page-one-box">
            <div className="dashboard-box">
              <div>{get(statistics, "allOrders")}</div>
              <p>Total orders</p>
            </div>
            <div className="dashboard-box">
              <div>{get(statistics, "shippedOrders")}</div>
              <p>Shipped item</p>
            </div>
            <div className="dashboard-box">
              <div>{get(statistics, "canceledOrders")}</div>
              <p>Backtracks</p>
            </div>
            <div className="dashboard-box">
              <div>{get(statistics, "totalReviews")}</div>
              <p>Total reviews</p>
            </div>
          </div>
          {get(orders, 'success.data.orderItems') && (
            <div className="content-ditalis">
            <div className="content-ditalis-one">
              <h3>Recent order status</h3>
              <div className="content-ditalis-one-left">
                <div className="submit-cl">
                  <div>
                    <div
                      className={`bef ${defBef === 1 ? "bef-block" : ""}`}
                    ></div>
                    <div
                      className={`def ${defBef === 1 ? "def-block" : ""}`}
                    ></div>
                  </div>
                  <div>
                    <h3>[Shenzhen] Data received</h3>
                  </div>
                </div>
                <div className="submit-cl">
                  <div>
                    <div
                      className={`bef ${defBef === 2 ? "bef-block" : ""}`}
                    ></div>
                    <div
                      className={`def ${defBef === 2 ? "def-block" : ""}`}
                    ></div>
                  </div>
                  <div>
                    <h3>Yo’lda</h3>
                  </div>
                </div>
                <div className="submit-cl">
                  <div>
                    <div
                      className={`bef ${defBef === 3 ? "bef-block" : ""}`}
                    ></div>
                    <div
                      className={`def ${defBef === 3 ? "def-block" : ""}`}
                    ></div>
                  </div>
                  <div>
                    <h3>Order picking & packing</h3>
                  </div>
                </div>
                <div className="submit-cl">
                  <div>
                    <div
                      className={`bef ${defBef === 4 ? "bef-block" : ""}`}
                    ></div>
                    <div
                      className={`def ${defBef === 4 ? "def-block" : ""}`}
                    ></div>
                  </div>
                  <div>
                    <h3>Awaiting order picking</h3>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-ditalis-two">
            <h3>Product details</h3>
              <Slider {...settings}>
                {get(orders, 'success.data.orderItems', []).map((order, index)=>(
                   <div key={index}>
                      <div className="content-ditalis-one-right">
                      <div className="sfnoislm-dawkjwdnawd">
                        <img
                          src={`${BASE_URL}${order['image']}`}
                          alt=""
                        />
                      </div>
                      <h2>
                        <span>{get(order, `productDetail.category.name.name_${lang}`)}</span>
                        <br />
                        {order['name']}
                      </h2>
                      <p>
                        {get(order, `productDetail.description.description_${lang}`)}
                      </p>
                      <div className="fot-size-color">
                        <div className="fot-size">
                          <div>Size:</div>
                          <div>{order['size']}</div>
                        </div>
                        <div className="fot-color">
                          <div>Color[{order['color']}]</div>
                          <div></div>
                        </div>
                      </div>
                    </div>
                 </div>
                ))}
              </Slider>
            </div>
          </div>
          )}
          </div>
      </div>
    </div>
  );
}

export default Dashboard;
