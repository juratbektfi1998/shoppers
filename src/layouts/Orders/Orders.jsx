import React, {useEffect, useState} from "react";
import ApiUser from "services/controller/user";
import "./Orders.scss";
import {MDBBtn, MDBContainer, MDBModal, MDBModalBody, MDBModalFooter, MDBModalHeader,} from "mdbreact";
import moment from "moment";

function Orders({cancelOrder, receiveOrder, complainOrder}) {
    const [state, setState] = useState({
        modal6: false,
        modal7: false,
        id: 0
    });

    const [order, setOrder] = useState(Array)
    const [file, setFile] = useState(null);

    useEffect(() => {
        getOrders()
    }, []);
    const [message, setMessage] = useState("");

    const toggle = (nr, id) => {

        let modalNumber = "modal" + nr;
        setState({
            ...state,
            [modalNumber]: !state[modalNumber],
            id
        });
        setMessage('')
    };

    const getOrders = () => {
        ApiUser.getAllOrders().then(value => setOrder(value.data.data)).catch(reason => window.print(reason))
    }

    const hundlChange = (e) => {
        setMessage(e.target.value);
    };

    function CancelOrder(id) {
        cancelOrder(id);
    }

    function ReceiveOrder(id) {
        receiveOrder(id);
        setMessage('')
        setFile(null)
        getOrders()
    }


    const ComplainOrder = () => {
        const formData = new FormData();
        formData.set('message', message)
        formData.set('order', state.id)
        if (file)
            formData.append(
                "image",
                file,
                file.name
            )

        complainOrder(formData);
        toggle(6, 0);
        setMessage('')
        setFile(null)
        getOrders()
    };

    const handle = (file) => {
        setFile(file)
    }

    return (
        <div>
            <div className="cart-page-one-1">
                <link
                    rel="stylesheet"
                    href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
                    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
                    crossorigin="anonymous"
                />
                <div className="my-cart-container">
                    <div className="product-menu">
                        <div>Adrress</div>
                        <div>STATUS</div>
                        <div>ARRIVED ON</div>
                        <div>Buttons</div>
                    </div>
                    <div>
                        {order.map((element) => {
                                return (
                                    <div key={element._id} className="product-one-list">
                                        <div>
                                            <div>
                                                <div className="wduadniuwh-center">{element.address.country + ','
                                                + element.address.city + ','
                                                + element.address.address}</div>
                                                <div
                                                    className="a-none">{moment(element.date).format("DD.MM.YYYY hh:mm")}</div>
                                            </div>

                                            <p>
                                                <p className="bg-color-bz">{element.status}</p>
                                                <div className="a-none">
                                                    {element.status === 'waiting' ? '' :
                                                        element.status !== 'in progress' ? (
                                                                <button
                                                                    type="button"
                                                                    class="btn btn-danger my-buuton-dcsd"
                                                                    onClick={() => toggle(6, element._id)}
                                                                >
                                                                    shikoyat
                                                                </button>
                                                            ) :
                                                            (
                                                                <div>
                                                                    <button
                                                                        type="button"
                                                                        class="btn btn-success my-buuton-dcsd"
                                                                        onClick={() => ReceiveOrder(element._id)}
                                                                    >
                                                                        qabul qildim
                                                                    </button>
                                                                </div>
                                                            )}
                                                </div>
                                            </p>
                                            <div className="b-none">{moment(element.date).format("DD.MM.YYYY hh:mm")}</div>
                                            <div className="b-none">
                                                {element.status === 'waiting' ? '' :
                                                    element.status !== 'in progress' ? (
                                                            <button
                                                                type="button"
                                                                class="btn btn-danger my-buuton-dcsd"
                                                                onClick={() => toggle(6, element._id)}
                                                            >
                                                                shikoyat
                                                            </button>
                                                        ) :
                                                        (
                                                            <div>
                                                                <button
                                                                    type="button"
                                                                    class="btn btn-success my-buuton-dcsd"
                                                                    onClick={() => ReceiveOrder(element._id)}
                                                                >
                                                                    qabul qildim
                                                                </button>
                                                            </div>
                                                        )}
                                            </div>
                                        </div>
                                    </div>
                                );
                            }
                        )}
                    </div>
                </div>
            </div>
            <MDBContainer>
                <MDBModal
                    // isOpen={state.modal6}
                    isOpen={state.modal6}
                    toggle={() => toggle(6)}
                    side
                    position="bottom-right"
                    className="audwh-dawiydgbhaw-dawvhdv"
                    backdropStyles={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end'
                    }}

                >
                    <MDBModalHeader toggle={() => toggle(6)}>Message</MDBModalHeader>
                    <MDBModalBody>
                        <div class="form-outline">
              <textarea
                  value={message}
                  onChange={hundlChange}
                  class="form-control"
                  id="textAreaExample"
                  rows="4"
                  name={"nameeee"}
              />
                            {file ? file.name : ''}
                            <div>
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                    <span
                                        className="input-group-text"
                                        id="inputGroupFileAddon01"
                                    >
                                      Upload
                                    </span>
                                    </div>
                                    <div className="custom-file">
                                        <input onChange={(event) => {
                                            handle(event.currentTarget.files[0]);
                                        }}
                                               type="file"
                                               className="custom-file-input"
                                               id="inputGroupFile01"
                                               aria-describedby="inputGroupFileAddon01"
                                        />
                                        <label
                                            className="custom-file-label"
                                            htmlFor="inputGroupFile01"
                                        >
                                            Choose file
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </MDBModalBody>
                    <MDBModalFooter>
                        <MDBBtn color="secondary" onClick={() => toggle(6)}>
                            Close
                        </MDBBtn>
                        <MDBBtn color="primary" onClick={ComplainOrder}>
                            Send message
                        </MDBBtn>
                    </MDBModalFooter>
                </MDBModal>
            </MDBContainer>
        </div>
    );
}

export default Orders;
