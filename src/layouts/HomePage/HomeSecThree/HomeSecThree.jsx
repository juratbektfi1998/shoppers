import { MDBAnimation, MDBCol, MDBContainer, MDBRow } from "mdbreact";
import React from "react";
import { useTranslation } from "react-i18next";
import "./HomeSecThree.scss";
import HomeTabOne from "./HomeTabOne/HomeTabOne";
import HomeTabTwo from "./HomeTabTwo/HomeTabTwo";
import SaleImg from "../../../assets/saleImg.png";
import { Link } from "react-router-dom";
import { getLang } from "utils/getLang";
import { BASE_URL } from "services/constants";

function HomeSecThree({ favorites, ratings, best }) {
  const { t } = useTranslation();
  const lang = getLang();
  let daysLeft = 10;
  let sold = 347;
  let progress = (daysLeft * 100) / sold;

  const _renderBest = () => {
    const {
      status,
      success: { data },
    } = best;
    if (status === "error") return <div>error</div>;
    if (status === "loading") return <div>loading</div>;
    return (
      data &&
      data.map((category, key) => (
        <div className="sel-item mb-2" key={key}>
          <Link
            to={`/productdetail/${category["category"]["category"]}/${category["_id"]}`}
          >
            <img
              src={
                BASE_URL + category["images"][0] ||
                "https://lp2.hm.com/hmgoepprod?set=quality[79],source[/11/36/11369e1ad16cffec548ec9f9ba98d810f4be2fa1.jpg],origin[dam],category[men_hoodiessweatshirts_hoodies],type[DESCRIPTIVESTILLLIFE],res[m],hmver[1]&call=url[file:/product/main]"
              }
              alt="xudy"
            />
            <div className="sale-item-info">
              <h5>{category["category"]["name"][`name_${lang}`]}</h5>
              <p>
                Started at:{" "}
                {`${category["purchasePrice"]} ${category["purchaseValute"]}`}
              </p>
              <p>Already sold: {category["sold"] || 0}</p>
            </div>
          </Link>
        </div>
      ))
    );
  };

  return (
    <div className="my-3 home-sec-three">
      <MDBContainer>
        <MDBRow>
          <MDBCol sm="6" md="3">
            <div className="left-box-sec">
              <div className="left-item">
                <div className="titul-top">
                  <h2>{t(`HomeSecThree.Titul`)}</h2>
                  <span className="sale-box">
                    <p>{t(`HomeSecThree.Titul1`)}</p>
                    $240
                  </span>
                </div>
                <img src={SaleImg} alt="imageseswfe" />
                <p>{t(`HomeSecThree.Titul2`)}</p>
                <div className="progress-box">
                  <div className="progress-top">
                    <div className="left-p">
                      {t(`HomeSecThree.Titul3`)}
                      {daysLeft}
                    </div>
                    <div className="right-p">
                      {t(`HomeSecThree.Titul4`)} {sold}
                    </div>
                  </div>
                  <div className="progress-item">
                    <div
                      className="in-progress"
                      style={{ width: `${progress}%` }}
                    />
                  </div>
                </div>
                <p className="last-text">* {t(`HomeSecThree.Titul5`)}</p>
              </div>
              <div className="best-sel mb-2">
                <h4>{t(`HomeSecThree.Titul6`)}</h4>
                {_renderBest()}
              </div>

              <div className="add-box">{t(`HomeSecThree.Titul7`)}</div>
            </div>
          </MDBCol>
          <MDBCol sm="6" md="9">
            <div className="right-box-sec mb-3">
              <div className="mb-3">
                {" "}
                <HomeTabOne favorites={favorites} />
              </div>
              <div>
                {" "}
                <HomeTabTwo ratings={ratings} />
              </div>
            </div>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </div>
  );
}

export default HomeSecThree;
