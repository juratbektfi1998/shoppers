import React from "react";
import "./HomeTabOne.scss";
import { getLang } from "utils/getLang";
import { Link } from "react-router-dom";
import { BASE_URL } from "services/constants";

function HomeTabOne({ favorites }) {
  const lang = getLang();

  function _renderCategories() {
    const { status, success } = favorites;
    if (status === "error") return <div>error</div>;
    if (status === "loading") return <div>loading</div>;

    return (
      <div className="home-tab-one">
        <div className="productslist">
          <div className="content">
            <h3 className="homeheader">ALL Products</h3>
            <div className="prodlist-tabs">
              <div className="prodlist">
                {success.data &&
                  success.data.map((category, key) => (
                    <div className="box" key={key}>
                      <Link
                        to={`/productdetail/${category["category"]["category"]}/${category["_id"]}`}
                      >
                        <div className="border" />
                        <div
                          className="container"
                          style={{
                            backgroundImage: `url('${BASE_URL}${category["images"][0]}')`,
                          }}
                        >
                          <h5>
                            {category.purchasePrice +
                              " " +
                              category.purchaseValute}
                          </h5>
                          <p>{category.category["name"][`name_${lang}`]}</p>
                        </div>
                      </Link>
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return _renderCategories();
}

export default HomeTabOne;
