import React from "react";
import "./HomeTabTwo.scss";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { getLang } from "utils/getLang";
import { BASE_URL } from "services/constants";

function HomeTabTwo({ ratings }) {
  const { t } = useTranslation();
  const lang = getLang();

  function _renderCategories() {
    const {
      status,
      success: { data },
    } = ratings;
    if (status === "error") return <div>error</div>;
    if (status === "loading") return <div>loading</div>;

    return (
      <div className="home-tab-two">
        <div className="productslist">
          <div className="content">
            <h3 className="homeheader">ALL Products 2</h3>
            <div className="prodlist-tabs">
              <div className="prodlist">
                {data &&
                  data.map((category, key) => (
                    <div className={`box box${key + 1}`}>
                      <Link
                        to={`/productdetail/${category["category"]["category"]}/${category["_id"]}`}
                      >
                        <div className="border" />
                        <div
                          className="container"
                          style={{
                            backgroundImage: `url('${BASE_URL}${category["images"][0]}')`,
                          }}
                        >
                          <h5>
                            {category["purchasePrice"] +
                              " " +
                              category["purchaseValute"]}
                          </h5>
                          <p>{category["category"]["name"][`name_${lang}`]}</p>
                        </div>
                      </Link>
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return _renderCategories();
}

export default HomeTabTwo;
