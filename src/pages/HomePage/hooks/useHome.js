import {useState, useEffect} from "react";
import useRequest from "hooks/useRequest";
import ApiCategory from "services/controller/category";
import ApiProduct from "services/controller/product";

const useHome = () => {
    const [categories, getCategories, categoriesDispatch] = useRequest();
    const [favorites, getFavorites] = useRequest();
    const [ratings, getRatings] = useRequest();
    const [best, getBest] = useRequest();

    useEffect(() => {
        (async () => await getCategories(ApiCategory.getAllCategory))();
        (async () => await getFavorites(ApiCategory.getAllFavorites))();
        (async () => await getRatings(ApiCategory.getAllRatings))();
        (async () => await getBest(ApiCategory.getBest))();
    }, []);

    return [categories, favorites, ratings, best];
};

export default useHome;
