//  scrollToTop = () => window.scrollTo(0, 0);

import React, { useEffect } from "react";
// NOTE hooks
import useHome from "pages/HomePage/hooks/useHome";

import HomeHeader from "components/HomeHeader/HomeHeader";
import TitleHemlet from "components/TitleHemlet/TitleHemlet";
import HomeSecFive from "layouts/HomePage/HomeSecFive/HomeSecFive";
import HomeSecFour from "layouts/HomePage/HomeSecFour/HomeSecFour";
import HomeSecThree from "layouts/HomePage/HomeSecThree/HomeSecThree";

import "./HomePage.scss";

function HomePage() {
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);
  const [categories, favorites, ratings, best] = useHome();

  return (
    <div className="home-page" style={{ backgroundColor: "rgba(0,0,0,0.04)" }}>
      <TitleHemlet title="Shoppers" />
      <HomeHeader categories={categories} />
      <HomeSecThree favorites={favorites} ratings={ratings} best={best} />
      <HomeSecFour />
      <HomeSecFive />
    </div>
  );
}

export default HomePage;
