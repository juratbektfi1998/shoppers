import { MDBBtn, MDBCol, MDBContainer, MDBRow } from "mdbreact";
import React, {Fragment, useEffect, useState, useContext} from "react";
import get from 'lodash.get'
import "./CheckOutPage.scss";

import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { useTranslation } from "react-i18next";
import TitleHemlet from "../../components/TitleHemlet/TitleHemlet";
import useCheckout from "./hook/useCheckout";
import { UserContext } from "context/user/context";

function CheckOutPage() {
  const [payment, setPayment] = useState('click');
  const [region, setRegion] = useState();
  const { t } = useTranslation();
  const [{ regions: {status, success}, CreateOrder, cards }] = useCheckout();
  const {state: {user}} = useContext(UserContext)

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  useEffect(()=>{
    setRegion(get(success, `data.regions[${0}]['region']`, ''))
  }, [success]);

  const initialValues = () => {
    const values = {
      streetAddress: '',
      phone: '',
      city: '',
      postcodeZip: '',
      state: '',
      shippingPrice: 0,
    }
    if (user.status === 'loading' || user.status === 'error') return values
    else {
      const {success} = user;
      return {
        ...values,
        streetAddress: get(success, 'address.address'),
        city: get(success, 'address.city'),
        phone: get(success, 'address.phonenumber'),
        postcodeZip: get(success, 'address.postalCode'),
        state: get(success, 'address.country'),
        shippingPrice: _getRegionsPrice() + _getSubTotal(),
      }
    }
  };

  const _getRegionsPrice = () => {
    const reg = get(success, 'data.regions', []).find(reg => reg['region'] === region);
    return get(reg, 'shippingPrice', 0);
  }

  const validationSchema = Yup.object().shape({
    streetAddress: Yup.string().required(t(`CheckOutPage.Titul20`)),
    city: Yup.string().required(t(`CheckOutPage.Titul20`)),
    phone: Yup.string()
      .min(7, t(`CheckOutPage.Titul22`))
      .max(20, t(`CheckOutPage.Titul23`))
      .required(t(`CheckOutPage.Titul20`)),
    postcodeZip: Yup.string().required(t(`CheckOutPage.Titul20`)),
    state: Yup.string().required(t(`CheckOutPage.Titul20`)),
  });

  const _renderCards = () => {
    const { status, success } = cards;
    if (status === 'loading') return 'Loading...'
    if (status === 'error') return 'Error'
    return get(success, 'data', []).map((card, index)=>(
                      <div className=" d-flex justify-content-between">
                        <p>{card['title']}</p>
                        <p>{card['valute'] === 'usd' ? `$ ${card['totalPrice']}` : `${card['totalPrice']} ${card['valute']}`}</p>
                      </div>
    ))
  }

  const _getSubTotal = () => {
    const { status, success } = cards;
    if (status === 'loading') return 'Loading...'
    if (status === 'error') return 'Error'
    

    return (
      <div>
        <p>$ {_getTotals(success)['totalInUsd']}</p>
        <p>{_getTotals(success)['totalInUzs']} uzs</p>
      </div>
    )
  }

  const _getTotals = (success) => {
    let totalInUsd = 0; 
    let totalInUzs = 0; 
    get(success, 'data', []).map(card => {
      if (card['valute'].toLowerCase() === 'usd') {
        totalInUsd += parseInt(card['totalPrice'])
      } else if (card['valute'].toLowerCase() === 'uzs') {
        totalInUzs += parseInt(card['totalPrice'])
      }
    })
    return {
      totalInUsd,
      totalInUzs,
    }
  }

  const _getTotalPrice = () => {
    const { status, success } = cards;
    if (status === 'loading') return 'Loading...'
    if (status === 'error') return 'Error'
    

    return (
      <div>
        <p>$ {_getTotals(success)['totalInUsd']}</p>
        <p>{_getTotals(success)['totalInUzs'] + _getRegionsPrice()} uzs</p>
      </div>
    )
  }

  if (status === "loading" || user.status === 'loading') return <h1>Loading...</h1>;
  if (status === "error") return <h1>Error...</h1>;
  console.log('outside', success)
  return (
    <div className="check-out-page">
      <TitleHemlet title="Check Out Page" />
      <MDBContainer>
        <div className="my-5 w-100 pt-5">
          <h1 className="w-100 text-center font-weight-bold">
            {t(`CheckOutPage.Titul`)}
          </h1>
        </div>
        <Formik
          initialValues={initialValues()}
          validationSchema={validationSchema}
          onSubmit={(values, actions) => {
            const { streetAddress, phone, city, postcodeZip, state} = values;
            const requestDto = {
              shippingAdress: {
                address: streetAddress,
                phonenumber: phone,
                city,
                postalCode: postcodeZip,
                country: state,
              },
              region: region,
              paymentMethod: payment
            };
            CreateOrder(requestDto);
          }}
        >
          {({ errors, touched, values }) => (
            <Form>
              <h3 className="text-uppercase font-weight-bold">
                {t(`CheckOutPage.Titul1`)}
              </h3>
              <MDBRow>
                <MDBCol md="8" sm="12">
                  <MDBRow>
                    <MDBCol md="8" sm="12">
                      <div className="mt-3">
                        <label htmlFor="streetAddress">
                          <small>
                            {/*<b>{t(`CheckOutPage.Titul7`)}</b>*/}
                            <b>Manzil</b>
                          </small>
                        </label>
                        <Field
                          className="form-control"
                          type="text"
                          id="streetAddress"
                          name="streetAddress"
                          placeholder={t(`CheckOutPage.Titul7`)}
                        />
                        <small style={{ color: "red" }}>
                          <ErrorMessage name="streetAddress">
                            {(msg) => <div>{msg}</div>}
                          </ErrorMessage>
                        </small>
                      </div>
                    </MDBCol>
                    <MDBCol md="4" sm="12">
                      <div className="mt-3">
                        {" "}
                        <label htmlFor="phone">
                          <small>
                            <b>{t(`CheckOutPage.Titul18`)}</b>
                          </small>
                        </label>
                        <Field
                          className="form-control"
                          type="phone"
                          id="phone"
                          name="phone"
                          placeholder={t(`CheckOutPage.Titul18`)}
                        />
                        <small style={{ color: "red" }}>
                          <ErrorMessage name="phone">
                            {(msg) => <div>{msg}</div>}
                          </ErrorMessage>
                        </small>
                      </div>
                    </MDBCol>
                  </MDBRow>
                  <MDBRow>
                    <MDBCol md="5" sm="12">
                      <div className="mt-3">
                        <label htmlFor="city">
                          <small>
                            <b>{t(`CheckOutPage.Titul16`)}</b>
                          </small>
                        </label>
                        <Field
                          className="form-control"
                          type="text"
                          id="city"
                          name="city"
                          placeholder={t(`CheckOutPage.Titul16`)}
                        />
                        <small style={{ color: "red" }}>
                          <ErrorMessage name="city">
                            {(msg) => <div>{msg}</div>}
                          </ErrorMessage>
                        </small>
                      </div>
                    </MDBCol>
                    <MDBCol md="7" sm="12">
                      <div className="mt-3">
                        {" "}
                        <label htmlFor="postcodeZip">
                          <small>
                            <b>{t(`CheckOutPage.Titul17`)}</b>
                          </small>
                        </label>
                        <Field
                          className="form-control"
                          type="text"
                          id="postcodeZip"
                          name="postcodeZip"
                          placeholder={t(`CheckOutPage.Titul17`)}
                        />
                        <small style={{ color: "red" }}>
                          <ErrorMessage name="postcodeZip">
                            {(msg) => <div>{msg}</div>}
                          </ErrorMessage>
                        </small>
                      </div>
                    </MDBCol>
                  </MDBRow>
                  <MDBRow>
                    <MDBCol md="6">
                      <div className="mt-3">
                        {" "}
                        <label htmlFor="state">
                          <small>
                            {/*<b>{t(`CheckOutPage.Titul9`)}</b>*/}
                            <b>Mamlakat</b>
                          </small>
                        </label>
                        <Field
                          className="form-control"
                          type="texxt"
                          id="state"
                          name="state"
                          placeholder={t(`CheckOutPage.Titul9`)}
                        />
                        <small style={{ color: "red" }}>
                          <ErrorMessage name="state">
                            {(msg) => <div>{msg}</div>}
                          </ErrorMessage>
                        </small>
                      </div>
                    </MDBCol>
                    <MDBCol md="6">
                      <div className="mt-3">
                        {" "}
                        <label htmlFor="viloyat">
                          <small>
                            {/*<b>{t(`CheckOutPage.Titul9`)}</b>*/}
                            <b>Viloyat</b>
                          </small>
                        </label>
                        <select style={{height: "39px"}} id="viloyat"
                                defaultValue={get(success, `data.regions[0]['region']`, 'Toshkent')}
                                onChange={(e) => setRegion(e.target.value)}
                                name="region" class="custom-select custom-select-sm">
                          {get(success, 'data.regions', []).map((region, index)=>(
                                <option key={index} value={region['region']}>{region['region']}</option>
                          ))}
                        </select>
                       <small style={{ color: "red" }}>
                          <ErrorMessage name="region">
                            {(msg) => <div>{msg}</div>}
                          </ErrorMessage>
                        </small>
                      </div>
                    </MDBCol>
                    <MDBCol md="12">
                      <div className="mt-3">
                        {" "}
                        <label htmlFor="payment">
                          <small>
                            {/*<b>{t(`CheckOutPage.Titul9`)}</b>*/}
                            <b>To`lov turini tanlang</b>
                          </small>
                        </label>
                        <select style={{height: "39px"}}
                                onChange={e => setPayment(e.target.value)}
                                id="payment" name="payment" class="custom-select custom-select-sm">
                                <option value="click">Click</option>
                                <option value="payme">Pay Me</option>
                        </select>
                         <small style={{ color: "red" }}>
                          <ErrorMessage name="region">
                            {(msg) => <div>{msg}</div>}
                          </ErrorMessage>
                        </small>
                      </div>
                    </MDBCol>
                  </MDBRow>
                  <div className="my-3">
                    <hr />
                  </div>
                </MDBCol>

                <MDBCol md="4" sm="12">
                  <div className="rounded border  p-4 mt-3">
                    <h4 className="text-uppercase font-weight-bold">
                      {t(`CheckOutPage.Titul26`)}
                    </h4>

                    <div className="d-flex justify-content-between">
                      <p className="font-weight-bold">
                        {t(`CheckOutPage.Titul27`)}
                      </p>
                      <p className="font-weight-bold">
                        {t(`CheckOutPage.Titul28`)}
                      </p>
                    </div>

                    <div>
                      {/* map buladigan joy */}
                      {_renderCards()}
                    </div>
                    <div className="mt-3 d-flex justify-content-between">
                      <p className="text-uppercase font-weight-bold">
                        {t(`CheckOutPage.Titul29`)}
                      </p>
                      {_getSubTotal()}
                    </div>
                    <div className=" d-flex justify-content-between">
                      <p className="text-uppercase font-weight-bold">
                        {t(`CheckOutPage.Titul30`)}
                      </p>
                      <div>
                        <small>{t(`CheckOutPage.Titul31`)}</small>
                        <p>{_getRegionsPrice()}</p>
                      </div>
                    </div>
                    <div className=" d-flex justify-content-between">
                      <p className="text-uppercase font-weight-bold">
                        {t(`CheckOutPage.Titul32`)}
                      </p>
                      <p>{_getTotalPrice()}</p>
                    </div>
                    <div className="mt-3 d-flex p-0">
                      <Field type="checkbox" id="readAgree" name="readAgree" />
                      <label className="ml-3 p-0" htmlFor="readAgree">
                        <small>
                          <b> {t(`CheckOutPage.Titul37`)}</b>
                        </small>
                      </label>
                    </div>

                    <div className="mt-3 w-100">
                      <MDBBtn
                        color="primary"
                        className="rounded-pill w-100"
                        rounded
                        type="submit"
                      >
                        {t(`CheckOutPage.Titul38`)}
                      </MDBBtn>
                    </div>
                  </div>
                </MDBCol>
              </MDBRow>
            </Form>
          )}
        </Formik>
      </MDBContainer>
    </div>
  );
}

export default CheckOutPage;
