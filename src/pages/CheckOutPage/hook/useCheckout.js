import React, {useEffect} from 'react';
import useRequest from "hooks/useRequest";
import ApiOrder from "services/controller/order";
import notification from "components/notification";
import { get } from 'react-hook-form';
import ApiCard from 'services/controller/card';

const useCheckout = () => {
    const [created, createOrder] = useRequest();
    const [regions, getRegions] = useRequest();
    const [cards, getCards] = useRequest();

    const CreateOrder = async (order) => {
        console.log('use', order);
        await createOrder(ApiOrder.createOrder, order);
    };

    useEffect(() => {
        (async () => await getRegions(ApiOrder.getRegions))();
        (async () => await getCards(ApiCard.getUsersCards))();
    }, []);

    useEffect(()=>{
        if (created.status === 'success') {
            window.location.replace(get(created, 'success.data.redirectUrl', '/myaccount'));
        }
        if (created.status === 'error') {
                notification.setMessage(created.error.error).setType('success').alert()
        }
    }, [created]);

    return [{regions, CreateOrder, cards}];
};

export default useCheckout;
