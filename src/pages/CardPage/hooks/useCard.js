import {useContext, useEffect} from "react";
import ApiCard from "services/controller/card";
import useRequest from "hooks/useRequest";
import {UserContext} from "context/user/context";
import get from 'lodash.get';
import notification from "components/notification";
import {actionTypes} from "../../../hooks/useRequest";
import ApiOrder from "services/controller/order";

const useCard = () => {
    const [cards, getUsersCards, dispatch] = useRequest();
    const [course, getValuteCourse] = useRequest();
    const [card, deleteCard] = useRequest();
    const [edited, editCardData] = useRequest();
    const {actions: {deleteCard: cardDelete}} = useContext(UserContext);
    const {
        actions: {getCardTotal},
    } = useContext(UserContext);

    useEffect(() => {
        (async () => await getUsersCards(ApiCard.getUsersCards))();
        (async () => await getValuteCourse(ApiOrder.getRegions))();
    }, []);

    const DeleteCard = (id) => {
        (async () => await deleteCard(ApiCard.deleteCard, id))();
    };

    const EditCardData = (id, action) => {
        (async () => await editCardData(ApiCard.editCardData, {id, action}))();
    };

    useEffect(() => {
        if (card.status === 'success') {
            const {success: {data}} = card;
            cardDelete(get(data, 'item._id'));
            dispatch({type: actionTypes.removeItem, item: get(data, 'item')})
            getCardTotal();
        } else if (card.status === 'error') {
            notification.setMessage('Something went wrong in useCard.js 31-line').setType('success').alert()
        }
    }, [card]);

    useEffect(() => {
        if (edited.status === 'success') {
            dispatch({
                type: actionTypes.changeState,
                payload: get(edited, 'success.data.item')
            })
            getCardTotal()
        } else if (edited.status === 'error') {
            notification.setMessage('Something went wrong with updating').setType('error').alert();
        }
    }, [edited]);

    return [{cards, DeleteCard, EditCardData, edited, course}];
};

export default useCard;
