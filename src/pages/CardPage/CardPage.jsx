import { MDBAnimation, MDBCol, MDBContainer, MDBRow } from "mdbreact";
import React, { useState, useEffect } from "react";
import TitleHelmet from "../../components/TitleHelmet/TitleHelmet";
import TitleHemlet from "../../components/TitleHemlet/TitleHemlet";
import OtherPageNav from "../../components/TopNav/OtherPageNav/OtherPageNav";
import CardPageLeft from "../../layouts/CardPage/CardPageLeft/CardPageLeft";
import CardPageRight from "../../layouts/CardPage/CardPageRight/CardPageRight";
import "./CardPage.scss";
import useCard from "./hooks/useCard";

function CardPage() {
  const [summ, setSumm] = useState(0);
  const [{ cards, DeleteCard, EditCardData, edited, course }] = useCard();

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  function onChangeSumm(newSumm) {
    setSumm((prevSum) => {
      return (prevSum += newSumm);
    });
  }

  return (
    <div>
      <TitleHelmet title="Card page" />

      <TitleHemlet title="Card Page" />

      <MDBContainer>
        <MDBRow>
          <MDBCol md="8" sm="12">
            <MDBAnimation type="fadeInLeft">
              <CardPageLeft
                editCard={EditCardData}
                cards={cards}
                deleteCard={DeleteCard}
                summ={summ}
                onChangeSum={onChangeSumm}
                edited={edited}
              />
            </MDBAnimation>
          </MDBCol>
          <MDBCol md="4" sm="12">
            <MDBAnimation type="fadeInRight">
              <CardPageRight cards={cards} course={course} />
            </MDBAnimation>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </div>
  );
}

export default CardPage;
