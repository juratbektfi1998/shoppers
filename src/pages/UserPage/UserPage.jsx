import React, {useEffect} from "react";

function UserPage() {
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);
  return <div>this is user page</div>;
}

export default UserPage;
