import {MDBAnimation, MDBCol, MDBContainer} from "mdbreact";
import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {Link} from "react-router-dom";
import TitleHemlet from "../../components/TitleHemlet/TitleHemlet";
import OtherPageNav from "../../components/TopNav/OtherPageNav/OtherPageNav";
import "./WishListPage.scss";
import useWishList from "./hooks/useWishList";
import {getLang} from "utils/getLang";
import {BASE_URL} from "services/constants";

function WishListPage() {
    const {t} = useTranslation();
    const lang = getLang();
    const [productCount, setProductCount] = useState(false);
    const [products, setProducts] = useState([]);
    const [{wishes: {status, success: {data}}, DeleteWish}] = useWishList();

    useEffect(() => {
        window.scrollTo({ top: 0, behavior: "smooth" });
      }, []);

    useEffect(() => {
        setProducts(data)
    }, [data]);

    const hundleClick = (id, item) => {
        if (item === 1) {
            products.forEach((element) => {
                if (element.id === id) element.count = element.count + 1;
            });
            setProductCount(!productCount);
            return;
        }
        if (item === 0) {
            products.forEach((element) => {
                if (element.id === id && element.count !== 0)
                    element.count = element.count - 1;
            });
            setProductCount(!productCount);
            return;
        }

        return;
    };

    if (status === 'error') return <p>error</p>;
    if (status === 'loading') return <p>loading</p>;

    return (
        <div>
            <TitleHemlet title="Wish List Page"/>
            <MDBAnimation type="fadeInUp">
                <MDBContainer>
                    <div className="my-5 w-100 pt-5">
                        <h1 className="w-100 text-center font-weight-bold">
                            {t(`WishListPage.Titul`)}
                        </h1>
                    </div>

                    <div className="d-flex justify-content-center">
                        <MDBCol md="10" sm="12" className="p-0">
                            <div className="cart-page-one-wish">
                                <link
                                    rel="stylesheet"
                                    href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
                                    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
                                    crossorigin="anonymous"
                                />
                                <div className="my-cart-container">
                                    <div className="product-menu">
                                        <div>{t(`WishListPage.Titul1`)}</div>
                                        <div>{t(`WishListPage.Titul2`)}</div>
                                        <div>{t(`WishListPage.Titul3`)}</div>
                                    </div>
                                    <div>
                                        {products && products.map((product) => {
                                            return (
                                                <div key={product['_id']} className="product-one-list">
                                                    <div>
                                                        <div>
                                                            <img
                                                                src={BASE_URL + product['images'][0]}
                                                                alt="wfefewfwe"
                                                            />
                                                        </div>
                                                        <h4>
                                                            <Link to={`productdetail/${product['category']['category']}/${product['_id']}`}>
                                                                {product['title']}
                                                            </Link>
                                                        </h4>
                                                        <p>
                                                            <p>{product['purchaseValute'].toLowerCase() === 'usd' ? ('$ ' + product['purchasePrice']) : (product['purchasePrice'] + ' ' + product['purchaseValute'])}</p>
                                                        </p>
                                                        <div className="tdaakjndjn">{product['quantity']}</div>
                                                        <div
                                                            onClick={() => DeleteWish(product['_id'])}
                                                             className="product-delete b-none">
                                                            <i className="fal fa-trash-alt"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                        </MDBCol>
                    </div>
                </MDBContainer>
            </MDBAnimation>
        </div>
    );
}

export default WishListPage;
