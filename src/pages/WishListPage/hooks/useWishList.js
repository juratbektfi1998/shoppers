import {useContext, useEffect} from "react";
import useRequest from "hooks/useRequest";
import {ApiWishList} from "services/controller/wishlist";
import {UserContext} from "context/user/context";
import get from 'lodash.get'

const useWishList = () => {
    const [wishes, getWishes] = useRequest();
    const [deletedWish, deleteWish] = useRequest();
    const {actions: {deleteWishItem}} = useContext(UserContext);

    useEffect(() => {
        (async () => await getWishes(ApiWishList.getWishList))();
    }, []);

    const DeleteWish = (id) => {
        (async () => await deleteWish(ApiWishList.deleteWish, id))();
    };

    useEffect(()=>{
        const {status, success} = deletedWish;
        if (status === 'success'){
            (async () => await getWishes(ApiWishList.getWishList))();
            deleteWishItem(get(success, 'data.product._id'))
        } else if(status === 'error'){
            alert('Something went wrong!')
        }
    }, [deletedWish]);

    useEffect(() => {
        const {status, success} = deletedWish;
        if (status === 'success') {
            const {data} = success;
            deleteWishItem(get(data, 'product._id'))
        } else if(status === 'error') {
            alert('Something went wrong!')
        }
    }, [deletedWish]);

    return [{wishes, DeleteWish}];
};

export default useWishList;
