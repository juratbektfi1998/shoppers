import {useEffect, useContext, useState, useCallback} from "react";
import {useHistory} from 'react-router-dom';
import useRequest from "hooks/useRequest";
import useArray, {actionTypes as arrayActionTypes} from "hooks/useArray";
import ApiUser from "services/controller/user";
import {UserContext} from "context/user/context";
import ApiComplaints from "services/controller/complaint";
import ApiOrder from "services/controller/order";
import notification from "components/notification";

const useAccount = () => {
    const [complaints, complaintsDispatch] = useArray();
    const [fileUrl, setUrl] = useState('.png')
    const history = useHistory();

    const {
        state: {user},
    } = useContext(UserContext);

    const [statistics, getStatisticsRequest] = useRequest();
    const [orders, getAllOrdersRequest] = useRequest();
    const [usersOrders, getUsersOrders] = useRequest();
    const [editedUser, editUserDetails] = useRequest();
    const [file, downloadFile] = useRequest();
    const [canceled, cancelOrder] = useRequest();
    const [received, receiveOrder] = useRequest();
    const [complained, complainOrder] = useRequest();

    useEffect(() => {
        (async () => await getStatisticsRequest(ApiUser.getStatistics))();
    }, []);

    useEffect(() => {
        const {status, success} = file;
        if (status === 'error') {
            notification.setMessage('counld not download file!').setType('error').request()
        }
        if(status === 'success') {
            const type = fileUrl.split('.').pop()
            const file = new Blob([success]);
              //Build a URL from the file
              const fileURL = URL.createObjectURL(file);
              //Open the URL on new Window
              // window.open(fileURL);
           
            // const blob = new Blob([success], {type:''})
            // const url = URL.createObjectURL(success);
            const a = document.createElement('a');
            a.style.display = 'none';
            a.href = fileURL;
            // the filename you want
            a.download =`ded.${type}`;
            document.body.appendChild(a);
            a.click();
            // URL.revokeObjectURL(url);
        }
    }, [file.status]);

    useEffect(()=>{
        const {status, error} = editedUser;
        if (status === 'error'){
            notification.setMessage(error.error).setType('error').alert()
        }
        if (status === 'success'){
            notification.setMessage('Your data has been successfully updated!').setType('success').alert()
            history.push('/myaccount')
        }
    }, [editedUser.status])

    const getAllOrders = async () =>
        await getAllOrdersRequest(ApiUser.getAllOrders);

    const getAllUsersOrders = async () =>
        await getUsersOrders(ApiOrder.getUsersOrders);

    const EditUserDetails = async (user) => {
        await editUserDetails(ApiUser.editAccountDetails, user)
    }    
    
    const getAllComplaints = async () => {
        complaintsDispatch({type: arrayActionTypes.request});

        const {data, success} = await ApiComplaints.getAllComplaints();

        if (success)
            return complaintsDispatch({
                type: arrayActionTypes.setInitial,
                list: data.data,
            });
        complaintsDispatch({
            type: arrayActionTypes.requestError,
            error: data.data,
        });
    };

    const DownloadFile = (url) => {
        setUrl(url);
        (async () => await downloadFile(ApiComplaints.getDownloadFile, url))();
    }

    const deleteComplaint = useCallback(async (id) => {
        const {data, success} = await ApiComplaints.delete(id);
        if (data["success"]) {
            complaintsDispatch({
                type: arrayActionTypes.remove,
                removedId: id,
                key: "_id",
            });
        } else {
            complaintsDispatch({type: arrayActionTypes.removeError});
            alert("delete fail");
        }
    }, []);

    const ReceiveOrder = async (id) => await receiveOrder(ApiOrder.receiveOrder, id);

    const CancelOrder = async (id) => await cancelOrder(ApiOrder.cancelOrder, id);

    const ComplainOrder = async (data) => await complainOrder(ApiOrder.complainOrder,data);

    return [
        statistics,
        orders,
        getAllOrders,
        complaints,
        user,
        getAllComplaints,
        deleteComplaint,
        CancelOrder,
        ReceiveOrder,
        ComplainOrder,
        DownloadFile,
        EditUserDetails,
        getAllUsersOrders,
        usersOrders,
    ];
};

export default useAccount;
