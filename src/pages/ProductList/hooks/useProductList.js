import {useEffect} from "react";
import {useParams} from "react-router-dom";
import useRequest from "hooks/useRequest";
import ApiCategory from "services/controller/category";
import ApiProduct from "../../../services/controller/product";

const useProductList = () => {
    const [categories, getCategories] = useRequest();
    const [products, getProducts] = useRequest();
    const {categoryId} = useParams();

    useEffect(() => {
        (async () => await getCategories(ApiCategory.getAllCategory))();
    }, []);

    useEffect(() => {
        if (categoryId) {
            (async () => await getProducts(ApiProduct.getAllByCategoryId, categoryId))();
        }
    }, [categoryId]);

    return [{categories, products}]
};

export default useProductList;
