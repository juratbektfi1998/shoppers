import React, {useContext, useEffect} from "react";
import ProductHeader from "../../layouts/ProductList/ProductHeader/ProductHeader";
import {useParams} from 'react-router-dom'
import ProductTab from "../../layouts/ProductList/ProductTab/ProductTab";
import ProductUnderTab from "../../layouts/ProductList/ProductUnderTab/ProductUnderTab";
import "./ProductList.scss";
import TitleHelmet from "../../components/TitleHelmet/TitleHelmet";
import useProductList from "./hooks/useProductList";
import {ProductContext} from "../../context/product/context";

function ProductList() {
    useEffect(() => {
        window.scrollTo({ top: 0, behavior: "smooth" });
      }, []);

    const [{categories, products}] = useProductList();
    const {categoryId} = useParams();
    const {state: {products: searchedProducts}} = useContext(ProductContext);

    return (
        <div className="product-list-page">
            <TitleHelmet title="Product List page"/>
            <ProductHeader categories={categories}/>
            <ProductTab products={categoryId ? products : searchedProducts}/>
            <ProductUnderTab/>
        </div>
    );
}

export default ProductList;
