import {useEffect} from "react";
import {useParams} from "react-router-dom";
import useRequest, { actionTypes } from "hooks/useRequest";
import ApiProduct from "services/controller/product";
import ApiCard from "services/controller/card";
import {ApiWishList} from "services/controller/wishlist";
import ApiComment from "services/controller/comment";
import notification from "components/notification";
import { get } from "react-hook-form";

const useDetail = () => {
    const [product, getProduct] = useRequest();
    const [comments, getComments, dispatch] = useRequest();
    const [relatedProducts, getRelatedProducts] = useRequest();
    const [createdComment, createComment] = useRequest();
    const [replyed, replyComment] = useRequest();
    const [card, addToCard] = useRequest();
    const [wish, addWishList] = useRequest();
    const {id} = useParams();
    const {categoryId} = useParams();

    useEffect(() => {
        (async () => await getRelatedProducts(ApiProduct.getAllRelated, categoryId))();
    }, [categoryId]);

    useEffect(()=>{
        (async () => await getProduct(ApiProduct.getById, id))();
    }, [id]);

    useEffect(()=>{
        const {status, success, error} = replyed
        if (status === 'success'){
            dispatch({type: actionTypes.changeItem, payload: get(success, 'data', {})})
        }
        if (status === 'error'){
            notification.setMessage(error.message).setType('error').alert();
        }
    }, [replyed.status]);

    useEffect(()=>{
        const {status, success, error} = createdComment
        if (status === 'success'){
            dispatch({type: actionTypes.addItem, payload: get(success, 'data', {})})
        }
        if (status === 'error'){
            notification.setMessage(error.message).setType('error').alert();
        }
    }, [createdComment]);

    const GetComments = async (productId) => {
        await getComments(ApiComment.getAllComments, productId);
    };

    const CreateComment = async (comment) => {
        await createComment(ApiComment.createComment, comment);
    };

    const ReplyComment = async (commentId, message) => {
        await replyComment(ApiComment.replyComment, {commentId, message});
    };

    const AddToCard = (product, qty, color, size) => {
        (async () => await addToCard(ApiCard.addCardData, {product, qty, color, size}))();
    };

    const AddToWishList = (productId) => {
        (async () => await addWishList(ApiWishList.addWish, productId))();
    };

    return [{product, relatedProducts, AddToCard, card, AddToWishList, wish, GetComments, comments, CreateComment, ReplyComment}];
};

export default useDetail;
