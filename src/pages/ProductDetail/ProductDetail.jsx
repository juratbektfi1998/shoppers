import {MDBAnimation, MDBCol, MDBContainer, MDBRow} from "mdbreact";
import React, {useContext, useEffect} from "react";
import get from "lodash.get";
import {useHistory} from "react-router-dom";
import TitleHemlet from "components/TitleHemlet/TitleHemlet";
import ProductDetailBottomSlider from "layouts/ProductDetail/ProductDetailBottomSlider/ProductDetailBottomSlider";
import ProductDetailLeftSlider from "layouts/ProductDetail/ProductDetailLeftSlider/ProductDetailLeftSlider";
import ProductDetailMidInfo from "../../layouts/ProductDetail/ProductDetailMidInfo/ProductDetailMidInfo";
import ProductDetailRightForm from "../../layouts/ProductDetail/ProductDetailRightForm/ProductDetailRightForm";
import "./ProductDetail.scss";
import useDetail from "./hooks/useDetail";
import {UserContext} from "context/user/context";
import notification from "../../components/notification";

/**
 * @return {null}
 */
function ProductDetail() {
    const [
        {product, relatedProducts, AddToCard, card, AddToWishList, wish, GetComments, comments, CreateComment, ReplyComment},
    ] = useDetail();
    const {
        actions: {addToCard, addToWishList, getCardTotal},
    } = useContext(UserContext);
    const {
        success: {data},
        status,
    } = product;

    const history = useHistory();

    useEffect(() => {
        window.scrollTo({ top: 0, behavior: "smooth" });
      }, []);

    useEffect(() => {
        if (card.status === "success") {
            const {
                success: {data},
            } = card;
            addToCard(data["item"]);
            getCardTotal()
            notification.setMessage('Product has been added in card.').setType('success').alert();
        }
        if (card.status === "error") notification.setMessage('something went wrong with adding product to card').setType('error').alert();
    }, [card.status]);

    useEffect(() => {
        if (wish.status === "success") {
            const {success: {data}} = wish;
            addToWishList(get(data, 'product._id'));
            notification.setMessage('Product has been added in wishlist.').setType('success').alert();
        }
        if (wish.status === "error") alert(wish.status);
    }, [wish.status]);

    if (status === "error") {
        setTimeout(() => {
            history.push("/");
        }, 2000);
        return <div>Could not find a product with this ID</div>;
    }
    if (status === "loading") return <div>loading</div>;

    return data ? (
        <div className="product-detail">
            <TitleHemlet title="Product Detail"/>
            <div className="paddingacontent"/>

            <MDBContainer>
                <MDBRow className="my-5">
                    <MDBCol sm="12" md="4" className="mb-3">
                        <MDBAnimation type="fadeInLeft">
                            <ProductDetailLeftSlider images={data["images"]}/>
                        </MDBAnimation>
                        <MDBAnimation type="fadeInRight">
                            <ProductDetailRightForm
                                data={data}
                                addToCard={AddToCard}
                                addWishList={AddToWishList}
                            />
                        </MDBAnimation>
                    </MDBCol>
                    <MDBCol sm="12" md="8" className="mb-3">
                        <MDBAnimation type="fadeInUp">
                            <ProductDetailMidInfo data={data} comments={comments} GetComments={GetComments} CreateComment={CreateComment} ReplyComment={ReplyComment}/>
                        </MDBAnimation>
                    </MDBCol>
                </MDBRow>

                <div className="mb-5">
                    <MDBAnimation reveal type="fadeInUp">
                        <ProductDetailBottomSlider related={relatedProducts}/>
                    </MDBAnimation>
                </div>
                <div className="mb-5">
                    <MDBAnimation reveal type="fadeInUp"/>
                </div>
            </MDBContainer>
        </div>
    ) : null;
}

export default ProductDetail;
