import React from "react";
import { Route, Switch } from "react-router-dom";
import AdminProtectedRoute from "./ProtectRoute";

import routes from "./route";

import HomePage from "./pages/HomePage/HomePage";
import ProductDetail from "./pages/ProductDetail/ProductDetail";
import OtherPage from "./layouts/OtherPage/OtherPage";
import ProductList from "./pages/ProductList/ProductList";
import FaqPage from "./pages/FaqPage/FaqPage";
import CardPage from "./pages/CardPage/CardPage";
import WishListPage from "./pages/WishListPage/WishListPage";
import CheckOutPage from "./pages/CheckOutPage/CheckOutPage";
import UserAccount from "./pages/UserAccount/UserAccount";
import MyAccount from "./pages/MyAccount/MyAccount";
import Login from "./components/FormComponent/Login/Login";
import Register from "./components/FormComponent/SignUp/SignUp";

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <div>
          {/* <Route exact path="/" component={HomePage} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route
            exact
            path="/productlist/:categoryId?"
            component={ProductList}
          />
          <Route
            exact
            path="/productdetail/:categoryId/:id"
            component={ProductDetail}
          />
          <AdminProtectedRoute path="/other" component={OtherPage} />
          <AdminProtectedRoute path="/faq" component={FaqPage} />
          <AdminProtectedRoute path="/card" component={CardPage} />
          <AdminProtectedRoute path="/wishlist" component={WishListPage} />
          <AdminProtectedRoute path="/checkout" component={CheckOutPage} />
          <AdminProtectedRoute path="/useraccount" component={UserAccount} />
          <AdminProtectedRoute path="/myaccount" component={MyAccount} /> */}

          {routes.map((route, index) => {
            if (route.private) {
              return <AdminProtectedRoute {...route} />;
            } else {
              return <Route {...route} />;
            }
          })}
        </div>
      </Switch>
    );
  }
}

export default Routes;
