const ADMIN='ADMIN';
const SELLER='SELLER';
const CUSTOMER='CUSTOMER';

export {
    ADMIN,
    SELLER,
    CUSTOMER
}