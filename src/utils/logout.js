import browserStorage from "services/browserStorage";

export const logout = () => {
  try{
    browserStorage.remove('token');
    browserStorage.remove('role')
    return true;
  }catch(e){
      return false;
  }
};


