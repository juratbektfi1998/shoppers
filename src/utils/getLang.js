import browserStorage from "services/browserStorage";

export const getLang = () => browserStorage.get("i18nextLng") || 'uz';
