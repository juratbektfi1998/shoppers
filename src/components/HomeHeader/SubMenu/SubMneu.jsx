import React, { useMemo } from "react";
import get from "lodash.get";
// Utils functions
import { getLang } from "utils/getLang";

import "./SubMenu.scss";
import { Link } from "react-router-dom";

function SubMneu({ categories, ...props }) {
  const lang = getLang();
  const menu = useMemo(() => {
    return get(categories, "success.data");
  }, [categories]);



  function _renderMenu(menuItem, key) {
    return (
      <div className="item-wrap" key={key}>
        <div className="item-header">
          <span>{get(menuItem, `name.name_${lang}`)}</span>{" "}
          <i className="fas fa-angle-right" />
          <div className="item-right">
            {get(menuItem, "childs", []).map((item, index) => (
              <div className="item-wrap2" key={index}>
                <div className="item-header2">
                  <span>{get(item, `name.name_${lang}`, [])}</span>{" "}
                  <i className="fas fa-angle-right" />
                  <div
                    className={
                      item.childs.length
                        ? "item-right2 dwiuhdgauibwjwea"
                        : "item-right2"
                    }
                  >
                    {get(item, "childs", []).map((item, index) => (
                      <p>
                        <Link to={`/productlist/${item["_id"]}`} key={index}>
                          {get(item, `name.name_${lang}`)}
                        </Link>
                      </p>
                    ))}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="submenu-box">
      {menu &&
        menu.map((menuItem, index) => {
          return _renderMenu(menuItem, index);
        })}
    </div>
  );
}

export default SubMneu;
