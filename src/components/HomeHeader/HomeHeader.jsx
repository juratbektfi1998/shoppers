import { MDBContainer } from "mdbreact";
import React from "react";
import { useTranslation } from "react-i18next";
import "./HomeHeader.scss";
import SliderBox from "./SliderBox/SliderBox";
import SubMneu from "./SubMenu/SubMneu";

function HomeHeader({ categories, ...props }) {
  const { t } = useTranslation();

  function _renderSubMenu() {
    if (categories.status === "error") return <div>error</div>;
    if (categories.status === "loading") return <div>loading</div>;

    return <SubMneu categories={categories} />;
  }

  return (
    <MDBContainer>
      <div className="home-header-box">
        <div className="left-header">
          <div className="top-box">
            <i className="fas fa-bars" />
            {t(`HomeHeader.Titul`)}
          </div>
          <div className="under-box">{_renderSubMenu()}</div>
        </div>

        <div className="right-header">
          <div className="under-box">
            <SliderBox />
          </div>
        </div>
      </div>

      <div className="mob-home-hedaer-box">
        <SliderBox />
      </div>
    </MDBContainer>
  );
}

export default HomeHeader;
