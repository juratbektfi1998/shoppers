import { toast } from 'react-toastify';

export const config = {
  position: 'top-right',
  autoClose: 3000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
};

class ApiNotification {
  type = 'success';
  delayTime = 0;
  message = '';

  setType(type) {
    this.type = type;
    return this;
  }

  setDelayTime(time) {
    this.delayTime = time;
    return this;
  }

  setMessage(message) {
    this.message = message;
    return this;
  }
  activate() {
    switch (this.type) {
      case 'success':
        toast.success(this.message, config);
        break;
      case 'error':
        toast.error(this.message, config);
        break;
      case 'warning':
        toast.warn(this.message, config);
      default:
        break;
    }
  }
  alert() {
    if (this.delayTime > 0) {
      setTimeout(() => {
        this.activate();
      }, this.delayTime);
    } else {
      this.activate();
    }
  }
}

const notification = new ApiNotification();

export default notification;
