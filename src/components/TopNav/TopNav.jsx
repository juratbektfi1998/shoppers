import React, { useEffect, useContext } from "react";
import TopNavOne from "./TopNavOne/TopNavOne";
import TopNavTwo from "./TopNavTWo/TopNavTwo";
import "./TopNav.scss";
import HomeMobHeader from "../HomeMobHeader/HomeMobHeader";
import { UserContext } from "context/user/context";
import { LoginContext } from "context/login/context";
function TopNav() {
  // if(user.status === 'loading') return <h1>Loading .....</h1>;

  // if(user.status === 'error') return <h1>Error ....</h1>

  return (
    <div className="top-nav">
      <TopNavOne />
      <TopNavTwo />
      <div className="home-mob-header">
        <HomeMobHeader />
      </div>
    </div>
  );
}

export default TopNav;
