import React, { useContext, useState } from "react";
import "./SearchBox.scss";
import { useTranslation } from "react-i18next";
import { ProductContext } from "context/product/context";

function SearchBox() {
  const [t] = useTranslation();
  const {
    actions: { search, changeHandler },
    state: { inputValue },
  } = useContext(ProductContext);



  return (
    <div className="adwuidhoaw-search">
      <form className="search-form" onSubmit={e=>e.preventDefault()}>
        <input
          type="text"
          placeholder={t(`MobSearch.Titul`)}
          value={inputValue}
          onChange={changeHandler}
        />
        <div className="right-box">
          <button type={"button"}>
            <i className="fa fa-search" onClick={search} />
          </button>
        </div>
      </form>
    </div>
  );
}

export default SearchBox;
