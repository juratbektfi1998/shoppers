import React, { Component, useState, useEffect } from "react";
import ReactStars from "react-rating-stars-component";
import {
  MDBCard,
  MDBCardBody,
  MDBRow,
  MDBCol,
  MDBListGroup,
  MDBListGroupItem,
  MDBAvatar,
  MDBBadge,
  MDBIcon,
  MDBBtn,
} from "mdbreact";
import get from 'lodash.get'
import "./index.scss";
import { useParams } from "react-router-dom";
import { BASE_URL } from "services/constants";
import moment from "moment";
// import { get } from "react-hook-form";

function ChatPage({GetComments, comments, CreateComment, ReplyComment}) {
  const [isViews, setIsViews] = useState(false);
  const [allText, setAllText] = useState(false);
  const [selectIndex, setSelectIndex] = useState(-1);
  const [allIndex, setAlltIndex] = useState(-1);
  const [state, setState] = useState({
    message: "",
    rating: 3,
    reating: "",
  });
  const [file, setFile] = useState(false);
  const params = useParams();
  const token = localStorage.getItem('token')

  useEffect(()=>{
    GetComments(params['id'])
  }, [])

  const hundleClick = (item) => {
    setIsViews(item == selectIndex ? !isViews : true);
    setSelectIndex(item);
  };

  const hundleFileChange = (e) => {
    setFile(e.target.files[0]);
  };

  const hundleChange = (e) => {
    const { name, value } = e.target;
    setState({
      ...state,
      [name]: value,
    });
  };

  const hundlDateSubmit = () => {
    if (state.message){
      console.log(file)
    const formData = new FormData()
    formData.set('rating', state['rating'])
    formData.set('product', params['id'])
    formData.set('message', state['message'])
    formData.append('image', file)
    CreateComment(formData)
    setFile(null)
    setState({...state, message: ""})
    }
  };

  const hundleTextClick = (item) => {
    setAllText(item == allIndex ? !allText : true);
    setAlltIndex(item);
  };

  const Alltext = (item, text) => {
    if (allText && item == allIndex) {
      return { text, longText: true };
    }

    return text && text.length > 100
      ? { text: text.slice(0, 99), longText: true }
      : { text, longText: false };
  };
  
  if (comments.status === 'loading') return <h1>Loading...</h1>
  if (comments.status === 'error') return <h1>Error...</h1>

  return (
    <MDBCardBody>
      <MDBRow className="px-lg-2 px-2">
        <MDBCol md="12" xl="12" className="px-0 mb-2 mb-md-0">
          <div className="white z-depth-1 p-3">
            {token && (
            <div className="form-group basic-textarea my-dwad-ad">
              <textarea
                className="form-control pl-2 my-0"
                id="exampleFormControlTextarea2"
                rows="3"
                required
                value={state.message}
                placeholder="Type your message here..."
                name="message"
                onChange={hundleChange}
              />
              <MDBBtn
                color="info"
                rounded
                size="sm"
                className="float-right mt-4"
                onClick={hundlDateSubmit}
              >
                Send
              </MDBBtn>
              <div className="startgridmessage">
                <div>
                  <label htmlFor="image">File Uploading</label>
                  <input
                    id="image"
                    type="file"
                    name="file"
                    onChange={hundleFileChange}
                  />
                </div>
                <ReactStars
                  value={3}
                  count={5}
                  onChange={(e) => setState({...state, rating: e})}
                  size={26}
                  activeColor="#ffd700"
                />
              </div>
            </div>
            )}
           <MDBListGroup className="friend-list">
              {get(comments, 'success.data', []).map((friend, index) => (
                <Friend
                  key={friend && friend.name}
                  friend={{
                    ...friend,
                    message: Alltext(index, friend.message),
                    active: isViews && selectIndex == index ? true : false,
                    when: friend['updatedAt'],
                  }}
                  hundleClick={hundleClick}
                  ReplyComment={ReplyComment}
                  index={index}
                  isViews={isViews}
                  selectIndex={selectIndex}
                  hundleTextClick={hundleTextClick}
                />
              ))}
            </MDBListGroup>
          </div>
        </MDBCol>
      </MDBRow>
    </MDBCardBody>
  );
}

const Friend = ({
  friend: { name, avatar, message, when, toRespond, seen, active, replies, image, _id, userDetail, rating: stars },
  hundleClick,
  index,
  isViews,
  selectIndex,
  ReplyComment,
  replay,
  hundleTextClick,
}) => {
    const [value, setValue] = useState('')
    const token = localStorage.getItem('token')

    const DoReply = () =>{
      if (value) {
        ReplyComment(_id, value)
        setValue("");
      }
    }
    return (
      <div className="friend-list-dawjdbawkdwq">
        <MDBListGroupItem
          href="#!"
          className="d-flex justify-content-between p-2 border-light"
          style={{ backgroundColor: active ? "#eeeeee" : "" }}
        >
          <img
            className="chat-img-asadwda"
            src={image ? (`${BASE_URL}${get(userDetail, 'profile_image')}`):('https://api.time.com/wp-content/uploads/2017/12/joey-degrandis-hsam-memory.jpg')}
            alt="Girl in a jacket"
            width="500"
            height="600"
          ></img>
          <div style={{ fontSize: "0.95rem", width: "70%" }}>
            <strong>{get(userDetail, 'name')}</strong>
            <p className="text-muted">{message.text}</p>
            <ReactStars
                  value={stars || 0}
                  count={5}
                  edit={false}
                  size={26}
                  activeColor="#ffd700"
                />
            <img
              className="chat-img-asadwda"
              src={image ? (`${BASE_URL}${image}`):('https://api.time.com/wp-content/uploads/2017/12/joey-degrandis-hsam-memory.jpg')}
              alt="Girl in a jacket"
              width="500"
              height="600"
            />
            <div className="viewallaa">
              <div className={message.longText ? "leftwieslddawdk" : ""}>
                {message.longText && (
                  <span onClick={() => hundleTextClick(index)}>all text</span>
                )}
              </div>
              {replies.length > 0 ? (
                <div>
                  <span onClick={() => hundleClick(index)}>views {replies.length}</span>
                </div>
              ) : null}
            </div>
          </div>
          <div>
            <p className="text-muted mb-0" style={{ fontSize: "0.75rem" }}>
              {moment(when).format("DD.MM.YYYY hh:mm")}
            </p>

            <span
              className="text-muted float-right"
              onClick={() => hundleClick(index)}
            >
              <MDBIcon icon="reply" aria-hidden="true" />
            </span>
          </div>
        </MDBListGroupItem>
        <div
          className={
            isViews && selectIndex == index ? "block-dip-dawaw" : "none-dip-dawdadw"
          }
        >
          <MDBListGroup className="friend-list friend-list-awdjonwkao">
            {token && (
              <div className="form-group basic-textarea my-dwad-ad">
              <textarea
                className="form-control pl-2 my-0"
                id="exampleFormControlTextarea2"
                rows="3"
                value={value}
                onChange={e => setValue(e.target.value)}
                placeholder="Type your message here..."
              />
              <MDBBtn color="info" rounded size="sm" onClick={DoReply} className="float-right mt-4">
                Send
              </MDBBtn>
              </div>
            )}
            {replies && replies.map((friend) => (
              <Friend2 key={friend.name} friend={{...friend, when: friend.updatedAt}} />
            ))}
          </MDBListGroup>
        </div>
      </div>
    );
}

const Friend2 = ({
  friend: { name, avatar, message, when, toRespond, seen, active, userDetail, image },
}) => (
  <MDBListGroupItem
    href="#!"
    className="d-flex justify-content-between p-2 border-light"
    style={{ backgroundColor: active ? "#eeeeee" : "" }}
  >
    <div style={{ fontSize: "0.95rem" }}>
      <img
              className="chat-img-asadwda"
              src={image ? (`${BASE_URL}${get(userDetail, 'profile_image')}`):('https://api.time.com/wp-content/uploads/2017/12/joey-degrandis-hsam-memory.jpg')}
              alt="Girl in a jacket"
              width="500"
              height="600"
            />
      <strong>{get(userDetail, 'name')}</strong>
      <p className="text-muted">{message}</p>
      <img
              className="chat-img-asadwda"
              src={image ? (`${BASE_URL}${image}`):('https://api.time.com/wp-content/uploads/2017/12/joey-degrandis-hsam-memory.jpg')}
              alt="Girl in a jacket"
              width="500"
              height="600"
            />
    </div>
    <div>
      <p className="text-muted mb-0" style={{ fontSize: "0.75rem" }}>
        {when}
      </p>
    </div>
  </MDBListGroupItem>
);

const ChatMessage = ({ message: { author, avatar, when, message } }) => (
  <li className="chat-message d-flex justify-content-between mb-4">
    <img
      className="chat-img-asadwda"
      src="https://api.time.com/wp-content/uploads/2017/12/joey-degrandis-hsam-memory.jpg"
      alt="Girl in a jacket"
      width="500"
      height="600"
    ></img>
    <MDBCard>
      <MDBCardBody>
        <div>
          <strong className="primary-font">{author}</strong>
          <small className="pull-right text-muted">
            <i className="far fa-clock" /> {when}
          </small>
        </div>
        <hr />
        <p className="mb-0">{message}</p>
      </MDBCardBody>
    </MDBCard>
  </li>
);

export default ChatPage;
