import React from "react";

function FooterSectionOne() {
  return (
    <div className="container">
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit id ullam
        fuga impedit quaerat labore vero nobis ipsa eveniet at similique
        inventore consectetur, totam accusamus natus enim repellendus! Quae a
        commodi quas atque, nam eligendi reprehenderit cupiditate aspernatur,
        necessitatibus id beatae fuga reiciendis aut voluptate? Minus ipsa neque
        optio esse nostrum alias itaque nisi repellendus reiciendis deleniti
        quas repudiandae quibusdam commodi rerum deserunt quaerat laudantium,
        perferendis provident et nihil, omnis mollitia! Ipsa assumenda illum sed
        vero. Molestias veniam sed explicabo quae non ut! Quidem aut doloremque
        nobis laborum quisquam molestias quaerat distinctio vitae rem suscipit
        modi aliquid dolorum similique mollitia odit repellendus, consequuntur
        dolor saepe quia iste. Nulla eius cumque necessitatibus aperiam labore
        placeat ipsum iste nihil cupiditate quia, a dignissimos voluptatibus
        fugiat unde minus, aliquid et excepturi quaerat repellat? Perspiciatis
        cumque neque quia dolorum? Doloribus, nam dicta et velit omnis facere
        nisi, molestiae in aspernatur, voluptate ipsam harum impedit consequatur
        necessitatibus. Reprehenderit cupiditate perspiciatis, a cumque quam
        iure alias accusamus in? Officiis a culpa sint? Explicabo eum vel eaque
        similique nam voluptas quia. Velit enim quibusdam at ipsam eum
        cupiditate repudiandae. Asperiores, rem? Hic, voluptate adipisci! Illum
        soluta ipsa assumenda neque, ut quasi quo. Officia excepturi dolorum
        fugit sit architecto quis odio quam, dignissimos incidunt ullam
        consequatur enim, doloremque ipsam tempora corporis a eum molestiae
        accusamus numquam at laborum nostrum repudiandae. Aperiam impedit
        corporis, cum soluta autem atque quisquam, fugiat at ipsa error non,
        magnam nemo. Fuga veritatis molestiae, quo cupiditate itaque impedit
        repudiandae nostrum voluptas omnis fugit. Iste asperiores unde,
        reiciendis ipsum saepe harum nobis! Voluptates labore unde quam fuga
        corporis, odit obcaecati in repudiandae, soluta neque illum est
        blanditiis dignissimos quo cum eveniet sit dolorum ipsam fugiat sequi!
        Facere sunt distinctio minima sapiente, numquam explicabo quas,
        perspiciatis quis reiciendis accusantium nemo. Dolorem tempore labore
        saepe autem alias accusantium minima, voluptates quia deleniti, neque
        rem, enim recusandae. Labore id sit corporis dolores fugiat a. Corrupti
        cum est adipisci? Facere modi aspernatur repudiandae quasi voluptatibus
        consectetur eaque quidem temporibus quo corrupti blanditiis, odio hic,
        dicta repellendus. Ex quaerat deleniti nisi tenetur, aliquam placeat
        repellat aut? Laborum asperiores eos impedit odit esse nisi voluptatum,
        blanditiis assumenda nostrum voluptas aspernatur. Accusantium iusto
        tempora alias tenetur in porro? Totam, expedita! Pariatur necessitatibus
        voluptate esse consequuntur facere, reiciendis sunt velit? Cumque
        doloribus nobis ipsam in quos delectus, veritatis adipisci blanditiis
        quibusdam tempore vel. Eum ipsam, magnam placeat maxime dolore sunt et
        modi rem veniam laborum molestiae facere nulla commodi nobis nam
        temporibus! Unde iste ipsa debitis eveniet perspiciatis fugit laboriosam
        nesciunt odio, officiis, obcaecati pariatur animi cumque quos aliquam
        necessitatibus hic quia dolores. Ea, in quibusdam. Perferendis natus,
        minus molestiae velit dolorem neque exercitationem dolore esse vero
        vitae odio illum provident? Consequuntur, accusantium incidunt quaerat
        maiores vero numquam labore, saepe ipsam sint eligendi facere voluptas
        consectetur odit dolorum velit autem nesciunt cumque fugiat nisi veniam
        repellendus quibusdam corrupti. Quisquam iure alias, laborum optio
        soluta recusandae molestias, similique nostrum perferendis qui ipsa
        repellendus. Quidem sunt rerum sapiente nisi! Vitae incidunt, quam a
        perferendis, fugiat accusantium laborum voluptate distinctio optio
        labore dignissimos nemo quod officiis quia debitis praesentium expedita
        nisi quidem. Iusto aperiam vitae, tempora modi inventore earum soluta
        accusantium? Numquam fugiat ad illum culpa, commodi pariatur iusto?
        Voluptate illo vitae molestiae doloribus laudantium. Aperiam cumque quia
        voluptates fugiat saepe sapiente modi eligendi nam vitae. Eum
        repudiandae excepturi, natus eaque deleniti unde voluptates quia,
        impedit dolorum ducimus saepe quo facilis incidunt at! Repellendus
        fugit, quibusdam aperiam inventore consequuntur at aut, blanditiis vero
        corporis necessitatibus, nobis voluptates quae et laudantium. Quam et
        doloribus voluptatem consequatur est corporis, iusto quae repellendus
        ipsa beatae voluptas placeat. Hic deserunt velit perferendis eligendi
        molestiae dignissimos voluptate explicabo, est dolor eveniet praesentium
        ipsa corrupti iure placeat, aliquam nihil? Vel nobis error, hic sequi
        dicta delectus quidem voluptatem quam consequuntur nesciunt commodi
        repellendus eaque. Dignissimos repellendus alias sequi nesciunt atque
        cupiditate enim quae natus, iste repudiandae non deleniti error
        incidunt! Officiis voluptates vel consectetur laboriosam, asperiores .
      </p>
    </div>
  );
}

export default FooterSectionOne;
