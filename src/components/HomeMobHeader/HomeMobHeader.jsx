import {
  MDBContainer,
  MDBCollapse,
  MDBCard,
  MDBCardBody,
  MDBCollapseHeader,
  MDBIcon,
} from "mdbreact";
import React, { useState, useEffect } from "react";
import "./HomeMobHeader.scss";
import MobSearch from "./MobSearch/MobSearch";
import Logo from "../../assets/shopLogo.svg";
import get from "lodash.get";
import Accordion from "./Accordion";
import ApiCategory from "services/controller/category";
import { Link } from "react-router-dom";
import useRequest from "hooks/useRequest";
import { getLang } from "utils/getLang";
function HomeMobHeader() {
  const [bg, setbg] = useState(true);

  const lang = getLang();

  const [categories, getCategories, categoriesDispatch] = useRequest();

  const { status, success } = categories;

  const [state, setState] = useState({
    collapseID: "collapse1",
  });

  useEffect(() => {
    getCategories(ApiCategory.getAllCategory);
  }, []);

  console.log(categories);

  return (
    <MDBContainer>
      <div
        className="bg-shadow"
        style={{
          opacity: `${bg ? "0" : "1"}`,
          zIndex: `${bg ? "-2222" : "999"}`,
          pointerEvents: `${bg ? "none" : "visible"}`,
        }}
        onClick={() => {
          setbg(!bg);
        }}
      />
      <div
        className="side-bar"
        style={{
          opacity: `${bg ? "0" : "1"}`,
          zIndex: `${bg ? "-2" : "9999"}`,
          left: `${bg ? "-100%" : "0"}`,
        }}
      >
        <div className="close-btn">
          <div
            className="icon-box"
            onClick={() => {
              setbg(!bg);
            }}
          >
            <i className="fas fa-times" />
          </div>
        </div>
        <img src={Logo} alt="logo" />

        <div>
          <div className="my-accardion-content my-hoverm-mobile">
            <Accordion
              className="accordion"
              selectedIndex={state.selectedIndex}
              onChange={(index, expanded, selectedIndex) => {}}
            >
              {status === "loading" ? (
                <p>menu loading..</p>
              ) : status === "error" ? (
                <p>menuni yuklashda error</p>
              ) : (
                get(success, "data", []).map((item) => (
                  <div data-header={get(item, `name.name_${lang}`)}>
                    <Accordion className="accordion my-bg-ayfdwvgaa">
                      {_renderMenu(item)}
                    </Accordion>
                  </div>
                ))
              )}
            </Accordion>
          </div>
        </div>
      </div>
      <div className="mob-header-box">
        <div
          className="left-item"
          onClick={() => {
            setbg(!bg);
          }}
        >
          <i className="fas fa-bars" />
        </div>

        <div className="right-item">
          <MobSearch />
        </div>
      </div>
    </MDBContainer>
  );

  function _renderMenu(item) {
    // console.log(item);

    if (!item.hasOwnProperty("childs")) {
      return null;
    }

    return item.childs.map((innerItem) => {
      return (
        <div
          data-header={get(innerItem, `name.name_${lang}`)}
          className="accordion-item"
        >
          {!innerItem.hasOwnProperty("childs") ? (
            <>
              <p>
                <Link to={`/productList/${innerItem['_id']}`}>{get(innerItem, `name.name_${lang}`)}</Link>
              </p>
            </>
          ) : (
            <></>
          )}
          {_renderMenu(innerItem)}
        </div>
      );
    });
  }
}

export default HomeMobHeader;
