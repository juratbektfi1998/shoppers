import React, {useContext, useState} from "react";
import "./MovSearch.scss";
import {useTranslation} from "react-i18next";
import {ProductContext} from "context/product/context";

function MobSearch() {
    const [t] = useTranslation();
    const [query, setQuery] = useState(null);
    const {actions: {search}} = useContext(ProductContext);

    const Search = () => {
        if (query) {
            search(query)
        }
    };

    return (
        <form action="#!">
            <div className="mob-search-box">
                <input type="text" placeholder={t(`MobSearch.Titul`)} onChange={(e) => setQuery(e.target.value)}/>
                <button type="submit" onClick={Search}>
                    <i className="fas fa-search"/>
                </button>
            </div>
        </form>
    );
}

export default MobSearch;
