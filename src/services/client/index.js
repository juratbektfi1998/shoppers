import get from "lodash.get";
import client from "services/api";

class ApiManagment {
  method = "GET";
  url = "";
  data = {};
  headers = {};
  additionalProperties = {};
  params = {};

  setMethod(method) {
    this.method = method;
    return this;
  }

  setUrl(url) {
    this.url = url;
    return this;
  }

  setData(data) {
    this.data = data;
    return this;
  }

  setHeaders(headers) {
    this.headers = headers;
    return this;
  }

  setAdditionalProperties(properties) {
    this.additionalProperties = properties;
    return this;
  }

  setParams(params) {
    this.params = params;
    return this;
  }

  async request() {
    try {
      const response = await client({
        method: this.method,
        url: this.url,
        data: this.data,
        headers: this.headers,
        ...this.additionalProperties,
      });
      return {
        data: get(response, "data"),
        success: true,
      };
    } catch (e) {
      return {
        data: get(e, "response.data"),
        success: false,
      };
    }
  }
}

const ApiManager = new ApiManagment();

export default ApiManager;
