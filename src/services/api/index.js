import axios from "axios";
import get from 'lodash.get';
import browserStorage from "services/browserStorage";
import { BASE_URL_API } from "services/constants";

const client = axios.create({
  baseURL: BASE_URL_API,
  headers: {
    Accept: "*/*",
    "Content-Type": "application/json",
  },
  responseType: "application/json",
});

client.interceptors.request.use(
  (config) => {
    config.headers.authorization = `Bearer ${browserStorage.get("token")}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// NOTE response interceptors -----

client.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    console.log(error.response);
    if (get(error, 'response.status') === 401) {
      browserStorage.remove("token");
      
      window.location.replace("/login");
    }
    let notification = error.response;
    return Promise.reject(error);
  }
);

export default client;
