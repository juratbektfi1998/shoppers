import ApiManager from "services/client";

const ApiUser = {
  async registerSeller(data) {
    return await ApiManager.setMethod("POST")
      .setUrl("/v1/auth/register-seller")
      .setData(data)
      .request();
  },
  async login(data) {
    return await ApiManager.setMethod("POST")
      .setUrl("/v1/auth/login")
      .setHeaders({
        'Content-Type':'application/json; charset=utf-8'
      })
      .setData(data)
      .request();
  },
  async registerPublic(data) {
    return await ApiManager.setMethod("POST")
      .setUrl("/v1/auth/register")
      .setData(data)
      .request();
  },
  async getStatistics() {
    return await ApiManager.setMethod("GET")
      .setUrl("/v1/users/get-statistics")
      .request();
  },
  async getAllOrders() {
    return await ApiManager.setMethod("GET")
      .setUrl("/v1/orders/my-orders ")
      .request();
  },
  async getUserDetail() {
    return await ApiManager.setMethod("GET").setUrl("/v1/auth/me").request();
  },
  async editAddress(body) {
    return await ApiManager.setMethod("PUT")
      .setUrl("/v1/auth/edit-address")
      .setData(body)
      .request();
  },
  async editAccountDetails(body) {
    return await ApiManager.setMethod("PUT")
      .setUrl("/v1/auth/edit")
      .setData(body)
      .setHeaders({
        "Content-Type":"multipart/form-data"
      })
      .request();
  },
  async getCardTotal() {
    return await ApiManager.setMethod("GET").setUrl("/v1/users/cart-total").request();
  },

};

export default ApiUser;
