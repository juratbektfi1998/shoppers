import ApiManager from "services/client";

const ApiOrder = {
    async receiveOrder(id) {
        return await ApiManager.setMethod('PUT').setUrl(`/v1/orders/receive/${id}`).request();
    },
    async cancelOrder(id) {
        return await ApiManager.setMethod("PUT").setUrl(`/v1/orders/cancel/${id}`).request();
    },
    async complainOrder(data) {
        return await ApiManager.setMethod("POST").setUrl(`/v1/complaints/create`)
            .setHeaders({
                'Content-Type':'multipart/form-data'
            })
            .setData(data).request();
    },
    async getRegions() {
        return await ApiManager.setMethod("GET").setUrl(`/v1/order-config/get`).request();
    },
    async createOrder(order) {
        return await ApiManager.setMethod("POST").setUrl(`/v1/orders/create`).setData(order).request();
    },
    async getUsersOrders() {
        return await ApiManager.setMethod("GET").setUrl(`/v1/orders/my-last`).request();
    }
};

export default ApiOrder
