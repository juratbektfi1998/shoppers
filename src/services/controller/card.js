import ApiManager from "services/client";

const ApiCard = {
    async getUsersCards() {
        return await ApiManager.setMethod('GET').setUrl('/v1/users/get-cart').request();
    },
    async addCardData({product, qty, color, size}) {
        return await ApiManager.setMethod("POST")
            .setUrl("/v1/users/add-cart")
            .setData({product, qty, color, size})
            .request();
    },
    async editCardData({id, action}) {
        return await ApiManager.setMethod("PUT")
            .setUrl(`/v1/users/edit-cart/${id}`)
            .setData({action})
            .request();
    },
    async deleteCard(id) {
        return await ApiManager.setMethod("DELETE")
            .setUrl(`/v1/users/remove-cart/${id}`)
            .request();
    }
};

export default ApiCard
