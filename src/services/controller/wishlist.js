import ApiManager from "services/client";

export const ApiWishList = {
    async getWishList() {
        return await ApiManager.setMethod('GET').setUrl('/v1/users/get-wishlist').request();
    },
    async addWish(product) {
        return await ApiManager.setMethod("POST")
            .setUrl("/v1/users/add-wishlist")
            .setData({
                product
            })
            .request();
        // Body:  { product: "productni _id si   —> string"
    },
    async deleteWish(productId) {
        return await ApiManager.setMethod("DELETE")
            .setUrl(`/v1/users/remove-wishlist/${productId}`)
            .request();
    }
};
