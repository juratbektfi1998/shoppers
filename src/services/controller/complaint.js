import ApiManager from "services/client";
import { BASE_URL } from "services/constants";

const ApiComplaints = {
  async getAllComplaints() {
    return await ApiManager.setMethod("GET")
      .setUrl("/v1/complaints/my-complaints ")
      .request();
  },
  async getDownloadFile(url) {
    return await ApiManager.setMethod("GET")
      .setUrl(`${BASE_URL}${url}`).setAdditionalProperties({responseType:'blob'})
      .request();
  },
  async delete(id) {
    return await ApiManager.setMethod("DELETE").setUrl(
      `/v1/complaints/delete/${id}`
    ).request();
  },
};

export default ApiComplaints;
