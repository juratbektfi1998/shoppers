import ApiManager from "services/client";


const ApiProduct = {
    async getById(id) {
        return await ApiManager.setMethod('GET').setUrl(`/v1/products/product/${id}`).request();
    },
    async getAllRelated(id) {
        return await ApiManager.setMethod('GET').setUrl(`/v1/products/all-related/${id}`).request();
    },
    async getAllByCategoryId(id) {
        return await ApiManager.setMethod('GET').setUrl(`/v1/products/all-related/${id}`).request();
    },
    async searchByName(arg) {
        let url=`/v1/products/search?q=${arg.q}`;
        if(arg?.var){
          url+=`&var=${arg.var}`
        }
        return await ApiManager.setMethod('GET').setUrl(url).request();
    }
};

export default ApiProduct;
