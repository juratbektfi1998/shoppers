import ApiManager from "services/client";


const ApiCategory = {
    async getAllCategory() {
        return await ApiManager.setMethod('GET').setUrl('/v1/category/all').request();
    },
    async getAllFavorites() {
        return await ApiManager.setMethod('GET').setUrl('/v1/products/all-favorits').request();
    },
    async getAllRatings() {
        return await ApiManager.setMethod('GET').setUrl('/v1/products/all-rating').request();
    },
    async getBest() {
        return await ApiManager.setMethod('GET').setUrl('/v1/products/bestsellers').request();
    }
};

export default ApiCategory;