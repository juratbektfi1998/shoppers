import ApiManager from "services/client";


const ApiComment = {
    async getAllComments(productId) {
        return await ApiManager.setMethod('GET').setUrl(`/v1/reviews/product/${productId}`).request();
    },
    async createComment(comment) {
        return await ApiManager.setMethod('POST').setUrl('/v1/reviews/create').setData(comment)
        .setHeaders({
            'Content-Type':'multipart/form-data'
        })
        .request();
    },
    async replyComment({commentId, message}) {
        return await ApiManager.setMethod('POST').setUrl(`v1/reviews/create-reply/${commentId}`)
        .setHeaders({
            'Content-Type':'application/json'
        })
        .setData({message}).request();
    }
};

export default ApiComment;