import { useReducer } from "react";
import get from "lodash.get";

export const requestActionTypes = {
  loading: "REQUEST",
  success: "REQUEST_SUCCESS",
  error: "REQUEST_ERROR",
  resetStatus: "RESET_REQUEST_STATUS",
};

export const actionTypes = {
  changeState: "CHANGE_STATE",
  addItem: "ADD_ITEM",
  changeItem: "CHANGE_ITEM",
  removeItem: "REMOVE_ITEM",
};

const initialState = {
  status: "",
  loading: {},
  success: {},
  error: {},
};

const requestReducer = (state, action) => {
  switch (action.type) {
    case requestActionTypes.loading:
      return {
        ...state,
        status: "loading",
      };
    case requestActionTypes.success:
      return {
        ...state,
        success: action.data,
        status: "success",
      };
    case requestActionTypes.error:
      return {
        ...state,
        error: action.fail,
        status: "error",
      };
    case actionTypes.changeState:
      const newItem = action.payload;
      const oldItem = get(state, "success.data").findIndex(
        (item) => item["_id"] === get(newItem, "_id")
      );
      const newList = get(state, "success.data");
      newList[oldItem]["qty"] = get(newItem, "qty");
      return {
        ...state,
        success: {
          data: newList,
        },
      };
    case actionTypes.addItem:
      const item = action.payload;
      return {
        ...state,
        success: {
          data: [item, ...get(state, "success.data", [])],
        },
      };
    case actionTypes.changeItem:
      // debugger;
      const Item = action.payload;
      const oldItemIndex = get(state, "success.data").findIndex(
        (item) => item["_id"] === get(Item, "_id")
      );
      const newArray = get(state, "success.data");
      newArray[oldItemIndex] = Item;
      return {
        ...state,
        success: {
          data: newArray,
        },
      };
    case actionTypes.removeItem:
      const removedItem = action.item
      const oldCardIndex = get(state, "success.data").findIndex(
        (item) => item["_id"] === get(removedItem, "_id")
      );
      const newCards = get(state, 'success.cart', []).splice(oldCardIndex, 1)
      return {
        ...state,
        success: {
          data: newCards,
        }
      }
    case requestActionTypes.resetStatus:
      return {
        ...state,
        status: "initial",
      };
    default:
      return state;
  }
};

const useRequest = () => {
  const [requestData, requestDispatch] = useReducer(
    requestReducer,
    initialState
  );

  const request = async (asyncFunction, arg) => {
    requestDispatch({ type: requestActionTypes.loading });

    const { success, data } = await asyncFunction(arg);

    if (success) {
      requestDispatch({ type: requestActionTypes.success, data });
    } else {
      requestDispatch({ type: requestActionTypes.error, fail: data });
    }
  };

  return [requestData, request, requestDispatch];
};

export default useRequest;
