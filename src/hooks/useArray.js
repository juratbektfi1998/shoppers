import { useReducer } from "react";

export const actionTypes = {
  request: "array/request",
  requestError: "array/requestError",
  setInitial: "array/setInitial",
  add: "array/add",
  remove: "array/remove",
  removeError: "array/removeError",
  reset: "array/reset",
  edit: "array/edit",
  changeStatus: "array/changeStatus",
};

const initilState = {
  status: "initial",
  list: [],
  error: {},
  loading: null,
};

const arrayReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.request:
      return {
        ...state,
        status: "loading",
      };
    case actionTypes.setInitial:
      const { list } = action;
      return {
        ...state,
        status: "success",
        list,
      };
    case actionTypes.requestError:
      return {
        ...state,
        status: "error",
      };
    case actionTypes.add:
      const { item } = action;
      return {
        ...state,
        list: [...state.list, item],
      };
    case actionTypes.remove:
      const { removedId, key } = action;

      let filteredData =
        state.list.filter((item) => item[key] !== removedId) || [];

      return {
        ...state,
        list: filteredData,
      };
    case actionTypes.removeError:
      return {
        ...state,
        success: "remove_item_error",
      };
    case actionTypes.changeStatus:
      return {
        ...state,
        status: action.status,
      };
    case actionTypes.reset:
      return initilState;
    default:
      return state;
  }
};

const useArray = () => {
  const [arr, arrDispatch] = useReducer(arrayReducer, initilState);

  return [arr, arrDispatch];
};

export default useArray;
