import React, {useContext, useMemo, useState} from "react";
import { Route, Redirect, useHistory } from "react-router-dom";
import browserStorage from "services/browserStorage";
import get from "lodash.get";
import { UserContext } from "context/user/context";
import { LoginContext } from "context/login/context";
const AdminProtectedRoute = ({ component: Component, roles, ...rest }) => {

  const {
    state: { user },
  } = useContext(UserContext);

  const {
      state:{loginData}
  }=useContext(LoginContext)

  const userRole = useMemo(() => get(user, "success.role", browserStorage.get('role') || get(loginData,'success.role')), [user,loginData]);

  function r(props) {

    if (roles.includes(userRole.toUpperCase())) {
      return <Component />;
    } else {
      return (
        <Redirect
          to={{
            pathname: "/",
            state: { from: props.location },
          }}
        />
      );
    }
  }

  return (
    <Route
      {...rest}
      render={(props) =>
        browserStorage.get("token") ? (
          r(props)
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

export default AdminProtectedRoute;
