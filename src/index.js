import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "react-tabs/style/react-tabs.css";
import "./index.css";
import App from "./App";
import "./i18n";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import registerServiceWorker from "./registerServiceWorker";
import Preloader from "./components/Preloader/Preloader";
import UserProvider from "context/user";
import ProductProvider from "./context/product/provider";
import LoginProvider from "context/login";
import { BrowserRouter as Router } from "react-router-dom";

ReactDOM.render(
  <Suspense fallback={<Preloader />}>
    <Router>
      <ToastContainer style={{ position: "fixed", zIndex: 1000 }} />
      <UserProvider>
        <ProductProvider>
          <LoginProvider>
            <App />
          </LoginProvider>
        </ProductProvider>
      </UserProvider>
    </Router>
  </Suspense>,

  document.getElementById("root")
);

registerServiceWorker();
